package controllers;

import models.AlbumsEntity;
import models.ArtistsEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

public class AlbumController {

    private EntityManagerFactory emf;
    public AlbumController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void create(AlbumsEntity album) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(album);
        em.getTransaction().commit();
        em.close();
    }
    public AlbumsEntity findByName(String albumName) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select t from AlbumsEntity t where t.name=:name");
        List albums = query.setParameter("name", albumName).getResultList();
        em.close();
        return albums.isEmpty() ? null : (AlbumsEntity) albums.get(0);
    }

    public Integer findidByName(String albumName) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select id from AlbumsEntity t where t.name=:name");
        List albums = query.setParameter("name", albumName).getResultList();
        em.close();
        return albums.isEmpty() ? null : (Integer) albums.get(0);
    }
}
