package com.java.ro;

public class Vehicle extends Item implements Asset{

    double performance;

    public Vehicle(String name,double performance, double price) {
        super(name, price);
        this.performance = performance;
    }

    @Override
    public double computeProfit() {
        return performance / price;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "performance=" + performance +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
