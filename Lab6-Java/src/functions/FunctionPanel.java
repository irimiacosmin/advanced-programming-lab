package functions;

import ekipa.Canvas;
import ekipa.ControlPanel;
import ekipa.Toolbar;

import javax.swing.*;
import java.awt.*;

public class FunctionPanel extends JFrame {

    FunctionCanvas functionCanvas;
    FunctionsControlPanel functionsControlPanel;

    public FunctionPanel() {
        super("Function Frame");
        init();
    }
    private void init() {

        setSize(new Dimension(700,500));

        setLayout(new BorderLayout());

        functionCanvas = new FunctionCanvas(this);
        add(functionCanvas,BorderLayout.NORTH);

        functionsControlPanel = new FunctionsControlPanel(functionCanvas);
        add(functionsControlPanel,BorderLayout.SOUTH);

        setVisible(true);

    }

}
