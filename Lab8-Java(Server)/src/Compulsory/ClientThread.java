package Compulsory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Random;

public class ClientThread extends Thread {
    private Socket socket = null;
    private final GameServer server;
    private boolean ready = false;
    private GuessingGame guessingGame = null;

    public ClientThread(GameServer server, Socket socket){
        System.out.println("Client (" + socket.getRemoteSocketAddress().toString() + ") connected.");
        this.server = server;
        this.socket = socket;
    }

    public void run() {
        while(true) {
            try {
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                String request = in.readLine();
                if (request.equalsIgnoreCase("exit")) {
                    socket.close();
                    server.getClientThreadList().remove(this);
                    System.out.println("Client (" + socket.getRemoteSocketAddress().toString() + ") disconnected.");
                    break;
                }
                String response = execute(request);
                if (ready == true) {
                    response = response + "$";
                }
                PrintWriter out = new PrintWriter(socket.getOutputStream());
                out.println(response);
                out.flush();
                if (ready == true) {
                    socket.close();
                    server.getClientThreadList().remove(this);
                    System.out.println("Client (" + socket.getRemoteSocketAddress().toString() + ") disconnected.");
                    break;
                }
            }catch (IOException e){
                e.printStackTrace();
            }

        }
    }
    private String execute(String request) throws IOException {
        String[] parts = request.toLowerCase().split(" ");

        if(parts.length>2){
            return "Too many arguments. Please use: <create [MAX_NUMBER]> or <submit [NUMBER]> or <quit>";
        }

        if(parts[0].equals("create") || parts[0].equals("c")){

            return createCommand(parts);
        }else if(parts[0].equals("submit") || parts[0].equals("s")){
            return submitCommand(parts);
        }
        else if(parts[0].equals("quit")){
            if(guessingGame == null){
                ready = true;
                return "Bye bye!";
            }else{
                ready = true;
                return "Loser, the number was "+ String.valueOf(guessingGame.number);
            }
        } else {
            return "Wrong command. Please use: <create [MAX_NUMBER]> or <submit [NUMBER]> or <quit>";
        }

    }

    private String createCommand(String[] parts){

        try {
            int aux = Integer.valueOf(parts[1]);

            if(aux<2){
                return "[MAX_NUMBER] is too small.";
            }else if(aux>999999){
                return "[MAX_NUMBER] is too big.";
            }
            guessingGame = new GuessingGame(this,socket.getRemoteSocketAddress().toString(),-1,0);
            Random random = new Random();
            guessingGame.number = random.nextInt(aux-1) + 1;
            return "A number between 1 and " + String.valueOf(aux) + " was generated. Now try to guess it!";
        }catch (Exception e){
            return "Wrong command. Please use: <create [MAX_NUMBER]>";
        }
    }

    private String submitCommand(String[] parts){

        if(guessingGame == null){
            return "[MAX_NUMBER] is not set. Please use <create [MAX_NUMBER]> first.";
        }

        try {
            int aux = Integer.valueOf(parts[1]);

            if(aux<guessingGame.number){
                guessingGame.attempts += 1;
                return "[NUMBER] " +parts[1] + " is too small.";
            }else if(aux>guessingGame.number){
                guessingGame.attempts += 1;
                return "[NUMBER] " +parts[1] + " is too big.";
            }else {
                guessingGame.attempts += 1;
                guessingGame.number = -1;
                ready = true;
                return "You got it in "+String.valueOf(guessingGame.attempts)+" attempts.";
            }

        }catch (Exception e){
            return "Wrong command. Please use: <submit [NUMBER]>";
        }

    }

}
