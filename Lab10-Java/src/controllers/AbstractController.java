package controllers;

import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
;import java.util.List;

@Repository
public abstract class AbstractController<T> {
    private EntityManagerFactory emf;
    private Object entity;
    protected Class<T> entityClass;
    public AbstractController(Class<T> controller){
        this.entityClass = controller;
    }
    public AbstractController(EntityManagerFactory emf, Object entity) {
        this.emf = emf;
        this.entity = entity;
    }
    public Object getEntity() {
        return entity;
    }
    public void create(T type) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(type.getClass());
        em.getTransaction().commit();
        em.close();
    }
    public T findByName(String name) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select t from " + entity.getClass().getName() +" t where t.name=:name");
        List<T> artists = query.setParameter("name", name).getResultList();
        em.close();
        return artists.isEmpty() ? null : artists.get(0);
    }
}