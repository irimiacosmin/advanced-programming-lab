package app;

import classes.*;
import controllers.*;


import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class OldAlbumManager {

    public OldAlbumManager() {
    }

    private void displayInfo(){
        System.out.println("\nINFO]");
        System.out.println("     -> create-artist [name]");
        System.out.println("     -> create-album  [albumName] [artistName]");
        System.out.println("     -> create-chart  [albumName_1] [albumName_2] ... [albumName_N]");
        System.out.println("     -> list-albums   [artistName]");
        System.out.println("     -> info\n");
    }

    public void run() throws IOException, SQLException, ClassNotFoundException {
        Database.getConnection();

        System.out.println("\n\n\n\n\n\n\n\n\n\n");
        displayInfo();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Input command: ");
            String command = scanner.nextLine();
            if (command.equals("exit")) break;
            String[] params = command.trim().split("\\s+");
            switch (params[0]) {
                case "create-artist":
                    if(params.length != 2){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createArtist(params[1]); //the artist name
                    break;
                case "create-album":
                    if(params.length != 3){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createAlbum(params[1], params[2]); //the album name and the artist name
                    break;
                case "create-chart":
                    if(params.length <1 ){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createChart(params); //the album name and the artist name
                    break;
                case "list-albums":
                    if(params.length != 2){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    listAlbums(params[1]); //the artist name
                    break;
                case "info":
                    displayInfo();
                    break;
                default:
                    System.out.println("[ERROR] Comand does not exists. Please use 'info' command.");
                    break;
            }
        }
    }
    private void createArtist(String artistName) throws SQLException, ClassNotFoundException {
        OldArtistController artistController = new OldArtistController();
        artistController.create(artistName,"null");
        Database.commit();
        System.out.println("\n     -> Artist "+artistName+" was created.\n");

    }
    private void createAlbum(String albumName, String artistName) throws SQLException, ClassNotFoundException {
        OldAlbumController albumController = new OldAlbumController();
        OldArtistController artistController = new OldArtistController();
        Integer id = artistController.findByNameLast(artistName);

        if(id != null) {
            albumController.create(id, albumName, 0);
            System.out.println("\n     -> Album with name: "+albumName+" was created.\n");
            Database.commit();
            return;
        }

        System.out.println("Artist "+ artistName + " does not exist. I cannot create album with name "+ albumName);

    }

    private void createChart(String[] params) throws IOException, SQLException, ClassNotFoundException {
        OldAlbumController albums = new OldAlbumController();
        EntityController entityController = new EntityController();
        Chart chart1 = new Chart();
        List<Album> albumList = new ArrayList<Album>();
        for(String str: params){
            try{
                Album album = new Album(albums.findByName(str),str,0);
                albumList.add(album);
                System.out.println("     -> Added "+album);
            }catch (Exception e){
                System.out.println("     -> Album "+str+" does not exist. I will just ignore it.");
            }
        }
        if(albumList.size() == 0){
            System.err.println("     -> Chart cannot be created.\n");
            return;
        }
        chart1.setAlbumList(albumList);
        entityController.createChart(chart1);
        Database.commit();
        System.out.println("     ->New chart has been created.");
    }

    private void listAlbums(String artistName) throws SQLException, ClassNotFoundException {
        OldAlbumController albums = new OldAlbumController();
        OldArtistController artistController = new OldArtistController();
        Integer id = artistController.findByNameLast(artistName);

        if(id != null) {
            System.out.println("All the albums from artist ("+id+" : "+artistName +"): ");
            albums.list(id);
            Database.commit();
            return;
        }

        System.out.println("\n     -> Artist with name: "+artistName+" does not exist.\n");

    }


}
