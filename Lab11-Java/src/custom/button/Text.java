package custom.button;

public @interface Text {
    String value() default "";
}
