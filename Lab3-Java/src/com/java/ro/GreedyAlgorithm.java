package com.java.ro;

import java.util.*;

public class GreedyAlgorithm implements Algorithm {

    public List<Item> computeSolution(List<Item> assetList, int maxValue){

        List<Item> bestPortofolio = new ArrayList<Item>();
        assetList = Utils.sortByPrice(assetList);
        int size = assetList.size();
        double[] profit = new double[size];
        double[] price = new double[size];
        double[] take   = new double[size];
        int j;

        for(j=0; j< size; j++)
        {
            profit[j] = ((Asset) assetList.get(j)).computeProfit();
            price[j] = ((Item)  assetList.get(j)).getPrice();
            take[j] = 0;
        }
        double total = maxValue;
        for (j = 0; j < profit.length; j++) {
            if (price[j] <= total) {
                take[j] = 1.00;
                total = total - price[j];
            } else {
                break;
            }
        }

        for(j=0; j< size; j++)
        {
            if(take[j]==1.0)
                bestPortofolio.add(assetList.get(j));
        }

        return bestPortofolio;
    }








}
