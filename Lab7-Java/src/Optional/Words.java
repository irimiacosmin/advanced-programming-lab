package Optional;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Words implements Dictionary {

    public static List<String> wordList;

    public Words(){
        wordList = new ArrayList<String>();
        readWords();
    }

    public static List<String> getWordList() {
        return wordList;
    }

    private static synchronized void readWords(){

        try(BufferedReader br = new BufferedReader(new FileReader("words\\words.txt"))) {
            for(String line; (line = br.readLine()) != null; ) {
                if(line.length()<8)
                    wordList.add(line.toLowerCase());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean containsWord(String str) {
        return wordList.contains(str);
    }
}
