package Compulsory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

public class GameServer {
    private static final int PORT = 8100;
    private ServerSocket serverSocket;
    private boolean running = false;
    private static List<ClientThread> clientThreadList;

    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Server started!");
        clientThreadList = new ArrayList<ClientThread>();
        GameServer server = new GameServer();
        server.init();
        server.waitForClients();

    }

    public void init() throws IOException{
        this.running = true;
        serverSocket = new ServerSocket(PORT);
        serverSocket.setSoTimeout(5000);
    }

    public void waitForClients() throws IOException, InterruptedException {
        System.out.println("Waiting for clients...");
        while(this.running){
            try {
                Socket socket = serverSocket.accept();
                clientThreadList.add(new ClientThread(this, socket));
                clientThreadList.get(clientThreadList.size() - 1).start();
            }catch (SocketTimeoutException s){
                System.out.println("Socket timed out!");
                stop();
            }
        }
    }

    public void stop() throws IOException, InterruptedException {

        System.out.println("Waiting all players to finish their games");
        while(clientThreadList.size()!=0)
        {
            Thread.sleep(1500);
        }

        this.running = false;
        serverSocket.close();
    }

    public List<ClientThread> getClientThreadList() {
        return clientThreadList;
    }

}