
import javax.swing.plaf.synth.SynthTextAreaUI;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Date;
import java.util.Scanner;

public class Optional {

    public Optional(String a){

        int n = Integer.parseInt(a);
        while(n%2==0){
            System.out.println("N is not odd");
            return;
        }
        long startTime = System.nanoTime();
        int[][] square = new int[n+1][n+1];
        System.out.println("========================================");
        if(n<=10) {

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {

                    square[i][j] = n * ((i + j + 1 + n / 2) % n) + (i + 2 * j + 1) % n + 1;

                    System.out.print(square[i][j] + " ");
                }
                System.out.println("");
            }
            System.out.println("========================================");
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {

                    square[i][j] = n * ((i + j + 1 + n / 2) % n) + (i + 2 * j + 1) % n + 1;


                    System.out.print(Character.toString((char)square[i][j]) + " ");
                }
                System.out.println("");
            }
            System.out.println("========================================");


            int sumLine = 0, sumCol = 0;
            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {

                    sumLine = sumLine + square[i][j];
                    sumCol = sumCol + square[j][i];

                }
                System.out.println("Sum on line " + i + " is: " + sumLine);
                System.out.println("Sum on column " + i + " is: " + sumCol);
                sumCol = 0;
                sumLine = 0;

            }

            int sumDiagP = 0, sumDiagS = 0;

            for (int i = 1; i < n; i++) {
                sumDiagP = sumDiagP + square[i][i];
                sumDiagS = sumDiagS + square[i][n - i + 1];
            }
            sumDiagS = sumDiagP = sumDiagP + square[n][n];

            System.out.println("Sum on Main Diagonal is: " + sumDiagP);
            System.out.println("Sum on Second Diagonal is: " + sumDiagS);
            if(sumDiagP==sumDiagS)
                System.out.println("MAGIC NUMBER is: " + sumDiagP);

        }
        else{

            for (int i = 1; i <= n; i++) {
                for (int j = 1; j <= n; j++) {

                    square[i][j] = n * ((i + j + 1 + n / 2) % n) + (i + 2 * j + 1) % n + 1;

                }
            }
            int sumDiagP = 0;
            for (int i = 1; i < n; i++) {
                sumDiagP = sumDiagP + square[i][i];

            }
            sumDiagP = sumDiagP + square[n][n];
            System.out.println("Magic number = "+sumDiagP);



            long endTime   = System.nanoTime();
            long totalTime = endTime - startTime;
            NumberFormat formatter = new DecimalFormat("#0.00000");
            System.out.println("Total time: "+formatter.format(totalTime / 1000000000d)+" seconds.");


        }





    }
}

/*

-Xms<size>        set initial Java heap size
-Xmx<size>        set maximum Java heap size
-Xss<size>        set java thread stack size

*/