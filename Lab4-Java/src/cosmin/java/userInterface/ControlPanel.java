package cosmin.java.userInterface;

import cosmin.java.commands.ReportCommand;
import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.Document;
import cosmin.java.lab4.MyException;
import cosmin.java.lab4.Utils;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ControlPanel extends JPanel implements Serializable {
    private final CatalogFrame frame;
    public final DocumentForm documentForm;
    public final ControlPanel controlPanel;
    JButton loadBtn = new JButton("Load");
    JButton saveBtn = new JButton("Save");
    JButton deleteBtn = new JButton("Delete");
    JButton openBtn = new JButton("Open");
    JButton treeBtn = new JButton("Tree");
    JButton reportBtn = new JButton("Report");
    JButton xmlBtn = new JButton("XML");
    JButton tableBtn = new JButton("Table");


    public ControlPanel(CatalogFrame frame,DocumentForm documentForm) {
        this.frame = frame;
        this.documentForm = documentForm;
        controlPanel = this;
        init();
    }
    private void init() {
        add(loadBtn);
        add(saveBtn);
        add(deleteBtn);
        add(openBtn);
        add(treeBtn);
        add(reportBtn);
        add(xmlBtn);
        add(tableBtn);

        loadBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser jfc = new JFileChooser(".//");
                jfc.setDialogTitle("Select a catalog to load ...");
                jfc.setAcceptAllFileFilterUsed(false);
                FileNameExtensionFilter filter = new FileNameExtensionFilter(".dat", "dat");
                jfc.addChoosableFileFilter(filter);

                int returnValue = jfc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    frame.list.removeAll();
                    CatalogFrame.catalog = new Catalog();
                    CatalogFrame.catalog.load(jfc.getSelectedFile().getPath());
                    for(Document document: CatalogFrame.catalog.getDocumentList())
                        frame.list.addDocument(document.title+", "+String.valueOf(document.year)+" ["+document.path+"]");
                }
            }
        });

        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String response = JOptionPane.showInputDialog(null,
                        "Please insert filename",
                        "Filename",
                        JOptionPane.QUESTION_MESSAGE);

                if(response.equals(null) || response.equals("") || response.equals(" ")){
                    JOptionPane.showMessageDialog(frame, "Filename cannot be null!");
                }
                else {
                    String filename = response.replaceAll("\\s+","");
                    CatalogFrame.catalog.save(filename+".dat");
                }

            }
        });

        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = frame.list.getIndex();
                if(selectedIndex == -1)
                { JOptionPane.showMessageDialog(frame, "No item is selected!");}
                else{
                    CatalogFrame.catalog.getDocumentList().remove(selectedIndex);
                    frame.list.removeDocument(selectedIndex);
                }
            }
        });


        openBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int selectedIndex = frame.list.getIndex();
                if(selectedIndex == -1)
                { JOptionPane.showMessageDialog(frame, "No item is selected!");}
                else {
                    CatalogFrame.catalog.open(CatalogFrame.catalog.getDocumentList().get(selectedIndex).path);
                }
            }
        });

        treeBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new TreeFrame(CatalogFrame.catalog, documentForm, frame, controlPanel);
            }
        });

        reportBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String response = JOptionPane.showInputDialog(null,
                        "Enter extension (TXT | DOC | PDF | XLS | HTML):",
                        "Extension",
                        JOptionPane.QUESTION_MESSAGE);

                if(response.equals(null) || response.equals("") || response.equals(" ")){
                    JOptionPane.showMessageDialog(frame, "Extension cannot be null!");
                }
                else if(response.toLowerCase().equals("xls") || response.toLowerCase().equals("pdf") ||response.toLowerCase().equals("txt") ||
                        response.toLowerCase().equals("doc") ||response.toLowerCase().equals("html")) {

                    List<String> arguments = new ArrayList<>();
                    arguments.add("report");
                    arguments.add(response.toLowerCase());
                    try {

                        new ReportCommand(arguments,CatalogFrame.catalog);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (MyException e1) {
                        e1.printStackTrace();
                    }

                }
                else {
                    JOptionPane.showMessageDialog(frame, "Extension is not suported!");
                }

            }
        });


        xmlBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String response = JOptionPane.showInputDialog(null,
                        "Please insert filename",
                        "Filename",
                        JOptionPane.QUESTION_MESSAGE);

                if(response.equals(null) || response.equals("") || response.equals(" ")){
                    JOptionPane.showMessageDialog(frame, "Filename cannot be null!");
                }
                else {
                    String filename = response.replaceAll("\\s+","");
                    try {
                        Utils.writeXML(CatalogFrame.catalog,filename+".xml");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });


        tableBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tableShow();
            }
        });

    }

    public void callBeibi(){
        new TreeFrame(CatalogFrame.catalog, documentForm, frame, controlPanel);
    }

    public void tableShow(){

        JFrame frame = new JFrame();
        Object rowData[][] = new Object[27][3];
        int i=0;
        for(Document document: CatalogFrame.catalog.getDocumentList()){
            rowData[i][0] = document.title;
            rowData[i][1] = document.path;
            rowData[i][2] = String.valueOf(document.year);
            i = i+1;
        }

        Object columnNames[] = { "Title", "Path", "Year"};
        JTable table = new JTable(rowData, columnNames);

        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(600,500);
        frame.setVisible(true);

    }

}
