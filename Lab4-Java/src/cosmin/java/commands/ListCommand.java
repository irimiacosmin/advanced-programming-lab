package cosmin.java.commands;

import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.MyException;

import java.util.List;

public class ListCommand implements ShellFace  {

    public Catalog catalog;
    public List<String> arguments;

    public ListCommand(Catalog catalog, List<String> arguments) throws MyException {
        this.arguments = arguments;
        this.catalog = catalog;

        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException {
        if(arguments.size() < 1)
        {
            throw new MyException("Too few arguments.");
        }
        catalog.list();
    }
}
