package cosmin.java.commands;

import cosmin.java.lab4.MyException;

import java.io.IOException;
import java.util.List;

public interface ShellFace {

    void computeCommand(List<String> arguments) throws MyException, IOException;

}
