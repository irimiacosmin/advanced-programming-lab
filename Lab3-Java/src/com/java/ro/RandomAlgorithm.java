package com.java.ro;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomAlgorithm implements Algorithm{

    public List<Item> computeSolution(List<Item> assetList, int maxValue){

        List<Item> bestPortofolio = new ArrayList<Item>();
        int currentCapacity = 0, i, visit = 0;
        int size = assetList.size();
        boolean[] visited = new boolean[size];
        Random rands = new Random();

        while(visit < size && currentCapacity < maxValue) {
            i = rands.nextInt(size);

            if(!visited[i]){
                visit++;
                visited[i] = true;

                if(assetList.get(i).getPrice() + currentCapacity <= maxValue){
                    currentCapacity += assetList.get(i).getPrice();
                    bestPortofolio.add(assetList.get(i));
                }
            }
        }

        return bestPortofolio;
    }
}


