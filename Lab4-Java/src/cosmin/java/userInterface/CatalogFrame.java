package cosmin.java.userInterface;

import cosmin.java.lab4.Catalog;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

    public class CatalogFrame extends JFrame implements Serializable {
    DocumentForm form;
    CatalogList list;
    ControlPanel control;
    public static Catalog catalog;

    public CatalogFrame() {
        super("Visual Document Manager");
        catalog = new Catalog();
        init();
    }
    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(600,500));

        setLayout(new BorderLayout());

        form = new DocumentForm(this);
        add(form, BorderLayout.NORTH);

        list = new CatalogList();
        add(list, BorderLayout.CENTER);

        control = new ControlPanel(this,form);
        add(control,BorderLayout.SOUTH);


    }

}
