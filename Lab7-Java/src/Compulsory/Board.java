package Compulsory;

import java.util.ArrayList;
import java.util.List;

public class Board {

    public List<String> words;

    public Board() {
        words = new ArrayList<String>();
    }

    public void addWord(Player player, String word){
        words.add(word);
        System.out.println("Player '"+player.getName()+"' added word "+word + " totaling "+player.getPoints()+" points.");
    }

}
