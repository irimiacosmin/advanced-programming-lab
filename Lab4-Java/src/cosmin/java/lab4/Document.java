package cosmin.java.lab4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Document implements Serializable {

    public String title;
    public String path;
    public int year;
    public List<String> authors;

    public Document(){}

    public Document(String title, String path, int year, String ... authors) throws MyException {

        String regularExpression = "([A-Z|a-z]:\\\\[^*|\"<>?\\n]*)|(\\\\\\\\.*?\\\\.*)";

        Pattern pattern = Pattern.compile(regularExpression);

        boolean isMatched = pattern.matches(regularExpression,path);

        if(isMatched==false)
            throw new MyException("\""+path+"\" is not a valid path.");
        if(year<1000 || year>2018)
            throw new MyException(year+" is not a valid year.");
        if(title.length()<3)
            throw new MyException("\""+title+"\" is not a valid title.");
        if(authors.length<1)
            throw new MyException("Authors cannot pe null.");

        this.title = title;
        this.path = path;
        this.year = year;
        this.authors = new ArrayList<String>();
        for (String author : authors)
            this.authors.add(author);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<String> getAuthors() {
        return authors;
    }

    public void setAuthors(List<String> authors) {
        this.authors = authors;
    }

    @Override
    public String toString() {
        return "Document{" +
                "title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", year=" + year +
                '}';
    }
}
