package functions;

import net.objecthunter.exp4j.ExpressionBuilder;
import org.apache.commons.math3.analysis.polynomials.PolynomialFunctionLagrangeForm;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;
import org.w3c.dom.DOMImplementation;


import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FunctionCanvas extends JPanel {

    public FunctionPanel functionPanel;
    PolynomialFunctionLagrangeForm polynomialFunctionLagrangeForm;
    public String expression = "x*0";
    public Polygon p = null;
    public Polygon poly = null;
    public boolean manualPlot = false;
    public boolean interpolateMode = false;
    public boolean startInterpolate = false;
    public boolean start = false;
    public boolean start2 = false;
    public ArrayList<Point> points;
    public int bigX,bigY;
    public double[] xs;
    public double[] ys;

    public FunctionCanvas(FunctionPanel functionPanel){
        this.functionPanel = functionPanel;
        points = new ArrayList<Point>();

        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {

                if(manualPlot){
                    addPoint(e.getX(),e.getY());
                    12.79787677144103*x^2+-8.7386459827732*x^1+1.387353798526342
                }
            }
        });
    }

    public void activateInterpolateMode(){
            int size = points.size();
            String exp = "";
                xs = new double[size];
                ys = new double[size];
                int i = 0;
                for(Point point: points){
                    xs[i] = point.x/100.0;
                    ys[i++] = point.y/100.0;
                }

                for(i=0;i<xs.length;i++)
                    System.out.println(xs[i]+" "+ys[i]);

                polynomialFunctionLagrangeForm = new PolynomialFunctionLagrangeForm(xs,ys);
                double coefficients[] = polynomialFunctionLagrangeForm.getCoefficients();


                for(i=0;i<coefficients.length;i++)
                    System.out.println(coefficients[i]);
                System.out.println("**********************");

                int sz = coefficients.length;
                for(i=0;i<sz;i++)
                    if((sz-i-1)!=0){
                        exp = exp + String.valueOf(coefficients[i]) + "*x^"+String.valueOf(sz-i-1)+" + ";
                    }
                    else {
                        exp = exp + String.valueOf(coefficients[i]);
                    }
                /*
                for(i=sz-1;i>=0;i--)
                    if(i!=0){
                        exp = exp + String.valueOf(coefficients[i]) + "*x^"+String.valueOf(i)+" + ";
                    }
                    else {
                        exp = exp + String.valueOf(coefficients[i]);
                    }
                */
                exp = exp.replaceAll("\\s+","");
                System.out.println(exp);
                functionPanel.functionsControlPanel.textField.setText(exp);
                drawInterpolation(exp);
                startInterpolate = true;

    }

    public void addPoint(int x, int y){
        poly = new Polygon();
        bigX = x;
        bigY = y;
        points.add(new Point(x, y));
        if (points.size() != 1) {
            for (Point point : points) {
                poly.addPoint(point.x, point.y);
            }
        }
        start = true;
    }

    public void setManualPlot(boolean manualPlot) {
        this.manualPlot = manualPlot;
    }

    public void drawInterpolation(String expression){
        this.expression = expression;
        p = new Polygon();
        for (int x = -320; x <= 320; x++) {
            p.addPoint(x + 335, 200 - (int) (50 * polynomialFunctionLagrangeForm.value( (x / 100.0) * 2 * Math.PI)));
        }
        start2 = true;
    }


    public void drawExpression(String expression){
        this.expression = expression;
        p = new Polygon();
        for (int x = -320; x <= 320; x++) {
            p.addPoint(x + 335, 200 - (int) (50 * myExpression( (x / 100.0) * 2 * Math.PI)));
        }
        start2 = true;
    }

    public Dimension getPreferredSize() {
        return new Dimension(700,400);
    }

    public double myExpression(double x){
        try {
            double result = new ExpressionBuilder(expression)
                    .variables("x")
                    .build()
                    .setVariable("x", x)
                    .evaluate();
            return result;
        }catch (Exception e){
            JOptionPane.showMessageDialog(this, "Expression is not valid!");
            functionPanel.setVisible(false);
            functionPanel = new FunctionPanel();
            expression = "x*0";

        }
        return 0;
    }

    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(2));
        g.drawLine(10, 200, 670, 200); // X AXIS
        g.drawLine(335, 10, 335, 400); // Y AXIS
        g.drawString("X", 670, 180);
        g.drawString("Y", 315, 20);
        g.drawString("0", 340, 195);
        g.setColor(Color.blue);

        if(manualPlot){
            if(start) {
                g.drawPolyline(poly.xpoints, poly.ypoints, poly.npoints);
                g.drawOval(bigX-1, bigY-1, 3, 3);
            }
            if(startInterpolate){
                g.setColor(Color.red);
                g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
            }
        }
        else{
            if(start2){
                g.drawPolyline(p.xpoints, p.ypoints, p.npoints);
            }
        }
        repaint();
    }

}
