import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Teacher implements Person {

    private String teacherID;
    private String name;
    private String surname;
    private String email;
    private Project project;
    private List<Student> preferences;
    private int capacity;

    public Teacher(String teacherID, String name, String surname, String email,int capacity) {
        this.teacherID = teacherID;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.capacity = capacity;
        preferences = new ArrayList<Student>();
    }

    @Override
    public String getTeacherID() {
        return teacherID;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Student> getPreferences() {
        return preferences;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setPreferences(Student ... student) {
        for(Student s:student)
            preferences.add(s);

    }

    public Project createProject(String name,int capacity){
        if(capacity > getCapacity())
            capacity = getCapacity();
        Project p = new Project(name,capacity);
        p.setPreferences(this.preferences);
        return p;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "teacherID='" + teacherID + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", project=" + project +
                ", preferences=" + preferences +
                '}';
    }

    @Override
    public String getStudentID() {
        return null;
    }

    public void setPreferences(List<Student> preferences) {
        this.preferences = preferences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(teacherID, teacher.teacherID) &&
                Objects.equals(name, teacher.name) &&
                Objects.equals(surname, teacher.surname) &&
                Objects.equals(email, teacher.email) &&
                Objects.equals(project, teacher.project) &&
                Objects.equals(preferences, teacher.preferences);
    }

    @Override
    public int hashCode() {

        return Objects.hash(teacherID, name, surname, email, project, preferences);
    }
}
