package ekipa;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class DrawingFrame extends JFrame {

    Canvas canvas;
    Toolbar toolbar;
    ControlPanel controlPanel;

    public DrawingFrame() {
        super("Drawing Frame");
        init();
    }
    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(new Dimension(900,700));

        setLayout(new BorderLayout());

        canvas = new Canvas();
        add(canvas, BorderLayout.CENTER);

        toolbar = new Toolbar(canvas);
        add(toolbar, BorderLayout.NORTH);

        controlPanel = new ControlPanel(canvas);
        add(controlPanel,BorderLayout.SOUTH);

        canvas.setToolbar(toolbar);
        canvas.setControlPanel(controlPanel);

    }





}
