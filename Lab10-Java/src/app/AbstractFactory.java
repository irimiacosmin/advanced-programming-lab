package app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

public class AbstractFactory {

    private boolean useJDBC = false;
    private boolean useJPA  = false;


    public AbstractFactory() {

    }

    private void parseIniFile() throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader("initialization.file"))) {
            String line;
            while ((line = br.readLine()) != null) {
                //System.out.println(line);
                String[] parts = line.split(" ");
                if(parts.length == 3) {
                    String part1 = parts[0];
                    String part2 = parts[1];
                    String part3 = parts[2];
                    if(part2.equals("=")){
                        if(part1.equals("use_JDBC")){
                            if(part3.equals("true")){
                                useJDBC = true;
                            }else if(part3.equals("false")){
                                useJDBC = false;
                            }
                        }
                        if(part1.equals("use_JPA")){
                            if(part3.equals("true")){
                                useJPA = true;
                            }else if(part3.equals("false")){
                                useJPA = false;
                            }
                        }
                    }
                }
            }
        }
    }

    public void run() throws IOException, SQLException, ClassNotFoundException {
        parseIniFile();

        if(useJDBC)
            new OldAlbumManager().run();
        else if(useJPA)
            new AlbumManager().run();


    }

}
