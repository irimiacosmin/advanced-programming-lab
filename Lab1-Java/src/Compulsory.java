

public class Compulsory {


    public int sumOfDigits(int n){

        int sum = 0;
        while(n!=0){
            sum = sum + n%10;
            n = n/10;
        }
        return sum;
    }

    public Compulsory(){

        System.out.println("Hello World!!");
        String[] languages = {"C", "C++", "C#", "Go", "JavaScript", "Perl", "PHP", "Python", "Swift", "Java"};
        int n = (int) (Math.random() * 1_000_000);
        n = n*3;
        n = n + Integer.parseInt("10101", 2);
        n = n + Integer.parseInt("FF", 16);
        n = n * 6;

        int result = sumOfDigits(n);

        while(result>9){
            result = sumOfDigits(result);
        }

        System.out.println("Willy-nilly, this semester I will learn " + languages[result]);



    }

}
