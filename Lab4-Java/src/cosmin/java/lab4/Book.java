package cosmin.java.lab4;

public class Book extends Document implements DocumentManager{

    public String publisher;

    public Book(){}

    public Book(String title, String path, int year,  String... authors) throws MyException {
        super(title, path, year, authors);
        this.publisher = null;
    }

    public Book(String title, String path, String publisher, int year,  String... authors) throws MyException {
        super(title, path, year, authors);
        this.publisher = publisher;
    }


    @Override
    public String toString() {
        return "Book{" +
                "publisher='" + publisher + '\'' +
                ", title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", year=" + year +
                ", authors=" + authors +
                '}';
    }

    @Override
    public void describe() {
        System.out.println("This is a Book description.");
    }
}
