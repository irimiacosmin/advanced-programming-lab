package com.example.irimi.lab12_java;

public class Event {

    private String name, details;
    private int year, month, day, hour, minute;

    public Event(String name, String details, int year, int month, int day, int hour, int minute) {
        this.name = name;
        this.details = details;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    @Override
    public String toString() {
        return "Name: "+name+"   " +
                "Date: "+hour+":"+minute+" "+day+"-"+month+"-"+year+"\n"+
                "Details: "+details;
    }
}
