import org.springframework.ui.Model;

public class Advanced {

    int[][] magicSquare;
    int i,j,num;
    int size;
    boolean ok = false;

    public Advanced(int size) {
        this.size = size;
    }

    private boolean validateSize(){
        if(size<3 || size>10000)
            return false;
        return true;
    }

    public void generateMatrix(){

        if(validateSize()==false){
            System.out.println("Size is not valid.");
            return;
        }

        if(size % 2 == 1)
            generateOddMatrix();
        else if (size % 4 ==0)
            generateDoubleEvenMatrix();
        else
            generateSingleEvenMatrix();

    }

    public void generateOddMatrix()
    {
        ok = true;
        magicSquare = new int[size][size];
        // Initialize position for 1
        int i = size/2;
        int j = size-1;

        // One by one put all values in magic square
        for (int num=1; num <= size * size; )
        {
            if (i==-1 && j==size) //3rd condition
            {
                j = size-2;
                i = 0;
            }
            else
            {
                //1st condition helper if next number
                // goes to out of square's right side
                if (j == size)
                    j = 0;

                //1st condition helper if next number is
                // goes to out of square's upper side
                if (i < 0)
                    i=size-1;
            }

            //2nd condition
            if (magicSquare[i][j] != 0)
            {
                j -= 2;
                i++;
                continue;
            }
            else
                //set number
                magicSquare[i][j] = num++;

            //1st condition
            j++; i--;
        }
    }

    public void generateDoubleEvenMatrix(){
            ok = true;
            int[][] magicsqr = new int[size][size];
            int N = magicsqr.length;

            int miniSqrNum = N/4; //size of boxes
            int cnt = 1; 	      //counter 1 to N*N
            int invCnt = N*N;     //counter N*N to 1
            for(int i=0;i<N;i++){

                for(int j=0;j<N;j++){

                    if(j>=miniSqrNum && j<N-miniSqrNum){
                        if(i>=miniSqrNum && i<N-miniSqrNum)
                            magicsqr[i][j] = cnt;    //central box
                        else
                            magicsqr[i][j] = invCnt; // up & down boxes

                    }
                    else if(i<miniSqrNum || i>=N-miniSqrNum){
                        magicsqr[i][j]=cnt;	         // 4 corners
                    }
                    else
                        magicsqr[i][j] = invCnt;  	// left & right boxes

                    cnt++;
                    invCnt--;
                }

            }
            magicSquare = magicsqr;
    }

    public void generateSingleEvenMatrix(){
        ok = true;
        int[][] magicsqr = new int[size][size];
        int N = magicsqr.length;
        int halfN = N/2; //size of ABCD boxes
        int k = (N-2)/4; // to get 'noses' of A & D boxes
        int temp;

        int [] swapCol = new int[N]; // columns which need to swap between C-B & A-D
        int index=0; // index of swapCol

        int [][] miniMagic =  new int [halfN][halfN];
        odd(miniMagic);	//creating odd magic square for A box

        //creating 4 magic boxes
        for (int i=0; i<halfN; i++)
            for (int j=0; j<halfN; j++){
                magicsqr[i][j] = miniMagic[i][j]; 	  		  //A box
                magicsqr[i+halfN][j+halfN] = miniMagic[i][j]+halfN*halfN;   //B box
                magicsqr[i][j+halfN] = miniMagic[i][j]+2*halfN*halfN;       //C box
                magicsqr[i+halfN][j] = miniMagic[i][j]+3*halfN*halfN;       //D box
            }



        for (int i=1; i<=k; i++)
            swapCol[index++] = i;

        for (int i=N-k+2; i<=N; i++)
            swapCol[index++] = i;

        //swaping values between C-B & A-D by known columns
        for (int i=1; i<=halfN; i++)
            for (int j=1; j<=index; j++){
                temp=magicsqr[i-1][swapCol[j-1]-1];
                magicsqr[i-1][swapCol[j-1]-1]=magicsqr[i+halfN-1][swapCol[j-1]-1];
                magicsqr[i+halfN-1][swapCol[j-1]-1]=temp;
            }

        //swaping noses
        temp=magicsqr[k][0];
        magicsqr[k][0]=magicsqr[k+halfN][0];
        magicsqr[k+halfN][0]=temp;


        temp=magicsqr[k+halfN][k];
        magicsqr[k+halfN][k]=magicsqr[k][k];
        magicsqr[k][k]=temp;
        //end of swaping

        magicSquare = magicsqr;

    }

    public void printMatrix(){
        if(ok)
            for(int i=0; i<size; i++)
            {
                for(int j=0; j<size; j++)
                    System.out.print(magicSquare[i][j]+" ");
                System.out.println();
            }
            else
                System.out.println("Please use generateMatrix() function first.");
    }

    public int[][] getMatrix(){
        if(ok)
            return magicSquare;
        System.out.println("Please use generateMatrix() function first.");
        return new int[1][1];
    }

    public boolean isMagicSqr(){
        if(ok == false)
        {
            System.out.println("Please use generateMatrix() function first.");
            return false;
        }

        int[][] magicsqr = magicSquare;
        int N = magicsqr.length;
        int magicConst = (N*N*N+N)/2;

        int rowsum = 0;
        int colsum = 0;
        int diag1 = 0;
        int diag2 = 0;
        for(int i=0;i<N;i++){
            for(int j=0;j<N;j++){
                rowsum += magicsqr[i][j];
                colsum += magicsqr[j][i];
            }
            diag1 += magicsqr[i][i];
            diag2 += magicsqr[i][N-i-1];
            if(rowsum!=magicConst){ return false; }
            if(colsum!=magicConst){ return false; }
            rowsum=0; colsum=0;
        }
        if(diag1!= magicConst || diag2 != magicConst)
            return false;

        return true;
    }

    private void odd(int[][] magic){

        int N = magic.length;
        int row = N-1;
        int col = N/2;
        magic[row][col] = 1;
        for(int i = 2; i<=N*N; i++){
            if(magic[(row+1)%N][(col+1)%N]== 0){
                row = (row+1)%N;
                col = (col+1)%N;
            }else{
                row = (row-1+N)%N;
            }
            magic[row][col] = i;
        }

    }
}
