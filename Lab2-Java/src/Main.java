
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Main {


    public static void main(String[] args){

        Ex1();

    }

    public static void Ex1(){




        Student s1 = new Student("S1","Irimia","Cosmin-Iulian", "s1@info.uaic.ro");
        Student s2 = new Student("S2","Bucevschi","Alexandru-Gabriel", "s2@info.uaic.ro");
        Student s3 = new Student("S3","Trinca","Ioana-Alexandra", "s3@info.uaic.ro");
        Student s4 = new Student("S4","Olariu","Madalina-Stefana", "s4@info.uaic.ro");

        Teacher t1 = new Teacher("T1", "Prelipcean","Bogdan","t1@info.uaic.ro",3);
        Teacher t2 = new Teacher("T2", "Ciobaca","Stefan","t2@info.uaic.ro",3);
        Teacher t3 = new Teacher("T3", "Patrut","Bogdan","t3@info.uaic.ro",2);


        Project p1 = t1.createProject("P1", 2);
        Project p2 = t2.createProject("P2", 1);
        Project p3 = t3.createProject("P3", 1);
        Project p4 = t3.createProject("P4", 2);

        s1.setPreferences(p1, p2, p3); // 1 3 1 1
        s2.setPreferences(p1, p3, p2);
        s3.setPreferences(p1);
        s4.setPreferences(p3, p2, p1);

        t1.setPreferences(s3, s1, s2, s4);
        t2.setPreferences(s1, s2, s3, s4);
        t3.setPreferences(s4, s3, s1, s2);


        System.out.println(s1);
        System.out.println(s2);
        System.out.println(s3);
        System.out.println(s4);
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
        System.out.println(p1);
        System.out.println(p2);
        System.out.println(p3);
        System.out.println("\n*********\nSolution:");


        Problem problem = new Problem();
        problem.setStudents(s1, s2, s3, s4);
        problem.setTeachers(t1, t2, t3);
        System.out.print(problem);
        System.out.println("*********\n");

        List<Person> participants = new ArrayList<Person>();
        participants = problem.getParticipants();

        List<Student> students = new ArrayList<Student>();
        students = problem.getStudentsList();

        List<Teacher> teachers = new ArrayList<Teacher>();
        teachers = problem.getTeachersList();

        System.out.println("Analysing student's preferences:");
        for(Student s:students)
            System.out.println(s.analyzeStud());

    }



}
