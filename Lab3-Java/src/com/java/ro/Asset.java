package com.java.ro;

public interface Asset {

    double computeProfit();
    default double riskFactor(){
        return 0.1;
    }

}
