package controllers;

import classes.Album;
import classes.Chart;
import models.AlbumsEntity;
import models.ArtistsEntity;
import models.ChartsEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ChartController {

    private EntityManagerFactory emf;
    public ChartController(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void create(List<AlbumsEntity> albumsEntities) throws IOException, SQLException {
        List<Album> albums = new ArrayList<Album>();
        System.out.println("\n Creating new chart with albums ... ");
        for(AlbumsEntity a: albumsEntities){
            Album album = getAlbum(a);
            if(album != null)
                albums.add(album);
        }
        Chart chart = new Chart(albums);
        ChartsEntity chartsEntity = new ChartsEntity();
        chartsEntity.setChart(write(chart));
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(chartsEntity);
        em.getTransaction().commit();
        em.close();
    }


    public Album getAlbum(AlbumsEntity albumsEntity){
        AlbumController albumController = new AlbumController(emf);
        String name = "default";
        Integer relase = 0, id=0;


        if(albumsEntity == null)
            return null;


        if(albumsEntity.getName() != null)
            name = albumsEntity.getName();
        else{
            name = "default";
        }
        if(albumsEntity.getReleaseYear() != null){
            relase = 0;
        }
        Album album = new Album(albumsEntity.getArtistId(),name,relase);
        System.out.println("     -> "+album);
        return album;
        //return new Album(1,"a",1);
    }

    public ChartsEntity getChartByID(int id) {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("select t from ChartsEntity t where t.id=:id");
        List charts = query.setParameter("id", id).getResultList();
        em.close();
        return charts.isEmpty() ? null : (ChartsEntity) charts.get(0);
    }

    private byte[] write(Object obj) throws SQLException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baos);
        oout.writeObject(obj);
        oout.close();
        return baos.toByteArray();
    }



}
