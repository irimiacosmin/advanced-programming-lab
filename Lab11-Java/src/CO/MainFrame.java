package CO;

import javax.swing.*;
import java.awt.*;
import java.io.Serializable;

public class MainFrame extends JFrame implements Serializable {
    ControlPanel controlPanel;
    DesignPanel designPanel;
    public MainFrame() {
        super("Swing Designer");
        init();
    }
    private void init() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);

        setSize(new Dimension(800,600));

        setLayout(new BorderLayout());

        controlPanel = new ControlPanel(this);
        designPanel = new DesignPanel(this);

        add(controlPanel, BorderLayout.NORTH);
        add(designPanel, BorderLayout.CENTER);
        pack();
        setVisible(true);

    }

    public static void main(String argv[]){
        new MainFrame();
    }
}