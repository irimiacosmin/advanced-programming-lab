package app;

import controllers.AlbumController;
import controllers.ArtistController;
import controllers.ChartController;
import models.AlbumsEntity;
import models.ArtistsEntity;
import models.ChartsEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class AlbumManager extends AbstractFactory{

    static final EntityManagerFactory emf = Persistence.createEntityManagerFactory("MusicAlbumsPU");
    static final EntityManager entityManager = emf.createEntityManager();


    public AlbumManager() {

    }

    private void displayInfo(){
        System.out.println("\nINFO]");
        System.out.println("     -> create-artist [name]");
        System.out.println("     -> create-album  [albumName] [artistName]");
        System.out.println("     -> create-chart  [albumName_1] [albumName_2] ... [albumName_N]");
        System.out.println("     -> list-albums   [artistName]");
        System.out.println("     -> info\n");
    }



    public void run() throws IOException, SQLException, ClassNotFoundException {
        System.out.println("\n\n\n\n\n\n\n\n\n\n");
        displayInfo();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Input command: ");
            String command = scanner.nextLine();
            if (command.equals("exit")) break;
            String[] params = command.trim().split("\\s+");
            switch (params[0]) {
                case "create-artist":
                    if(params.length != 2){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createArtist(params[1]); //the artist name
                    break;
                case "create-album":
                    if(params.length != 3){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createAlbum(params[1], params[2]); //the album name and the artist name
                    break;
                case "create-chart":
                    if(params.length <1 ){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    createChart(params); //the album name and the artist name
                    break;
                case "list-albums":
                    if(params.length != 2){
                        System.out.println("[ERROR] Syntax error.");
                        break;
                    }
                    listAlbums(params[1]); //the artist name
                    break;
                case "info":
                    displayInfo();
                    break;
                default:
                    System.out.println("[ERROR] Comand does not exists. Please use 'info' command.");
                    break;
            }
        }
    }
    private void createArtist(String artistName) {
        ArtistController artistController = new ArtistController(emf);
        ArtistsEntity artistsEntity = new ArtistsEntity();
        artistsEntity.setName(artistName);
        artistController.create(artistsEntity);
        System.out.println("\n     -> Artist "+artistName+" was created.\n");
    }
    private void createAlbum(String albumName, String artistName) {
        entityManager.getTransaction().begin();
        AlbumsEntity albumsEntity = new AlbumsEntity();
        //albumsEntity.setArtistId(4);
        ArtistsEntity a = findArtistByName(artistName);
        if(a == null){
            System.out.println("Artist "+ artistName + " does not exist. I cannot create album with name "+ albumName);
            entityManager.getTransaction().commit();
            return;
        }
        albumsEntity.setArtistId(a.getId());
        albumsEntity.setName(albumName);
        entityManager.persist(albumsEntity);
        entityManager.getTransaction().commit();
        System.out.println("\n     -> Album with name: "+albumName+" was created.\n");
    }

    private void createChart(String[] params) throws IOException, SQLException {
        ChartController chartController = new ChartController(emf);
        AlbumController albumController = new AlbumController(emf);
        List<AlbumsEntity> albumsEntities = new ArrayList<AlbumsEntity>();

        for(String str: params){
            try{
                AlbumsEntity albumsEntity = albumController.findByName(str);
                albumsEntities.add(albumsEntity);
            }catch (Exception e){
                System.out.println("Album "+str+" does not exist. I will just ignore it.");
            }
        }
        if(albumsEntities.size() == 0){
            System.err.println("     -> Chart cannot be created.\n");
            return;
        }

        chartController.create(albumsEntities);
        System.out.println("\n     ->New chart has been created.");
    }

    private ArtistsEntity findArtistByName(String artistName){
        ArtistController artistController = new ArtistController(emf);
        return artistController.findByName(artistName);
    }

    private void listAlbums(String artistName) {
        ArtistsEntity artistsEntity = findArtistByName(artistName);

        List<AlbumsEntity> albumList =
                entityManager.createQuery("select a from AlbumsEntity a where a.artistId = "+artistsEntity.getId())
                        .getResultList();
        System.out.println("\n Albums for artist "+artistName);
        for(AlbumsEntity albumsEntity: albumList){
            System.out.println("     -> "+albumsEntity);
        }
        System.out.println("\n");
    }



}
