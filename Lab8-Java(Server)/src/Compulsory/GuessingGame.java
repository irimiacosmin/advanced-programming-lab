package Compulsory;

public class GuessingGame {

    public String player;
    public int number;
    public int attempts;
    private ClientThread clientThread;

    public GuessingGame(ClientThread clientThread, String player, int number, int attempts) {
        this.clientThread = clientThread;
        this.player = player;
        this.number = number;
        this.attempts = attempts;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public ClientThread getClientThread() {
        return clientThread;
    }

    public void setClientThread(ClientThread clientThread) {
        this.clientThread = clientThread;
    }
}
