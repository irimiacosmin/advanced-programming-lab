package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "test", schema = "MusicAlbums", catalog = "")
public class TestEntity {
    private int idtest;

    @Id
    @Column(name = "idtest")
    public int getIdtest() {
        return idtest;
    }

    public void setIdtest(int idtest) {
        this.idtest = idtest;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestEntity that = (TestEntity) o;
        return idtest == that.idtest;
    }

    @Override
    public int hashCode() {

        return Objects.hash(idtest);
    }
}
