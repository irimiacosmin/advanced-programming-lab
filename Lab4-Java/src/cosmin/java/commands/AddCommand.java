package cosmin.java.commands;

import cosmin.java.lab4.*;

import java.util.List;

public class AddCommand implements ShellFace {

    public Catalog catalog;
    public List<String> arguments;

    public AddCommand(Catalog catalog, List<String> arguments) throws MyException {
        this.arguments = arguments;
        this.catalog = catalog;
        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException {

        String objectType = arguments.get(1);

        if(arguments.size() < 6)
        {
            throw new MyException("Too few arguments.");
        }

        if(objectType.toLowerCase().equals("book")){
            catalog.add(new Book(arguments.get(2),arguments.get(3), Integer.parseInt(arguments.get(4)),arguments.get(5)));
        }else if(objectType.toLowerCase().equals("article")){
            catalog.add(new Article(arguments.get(2),arguments.get(3), Integer.parseInt(arguments.get(4)),arguments.get(5)));
        }else if(objectType.toLowerCase().equals("manual")){
            catalog.add(new Manual(arguments.get(2),arguments.get(3), Integer.parseInt(arguments.get(4)),arguments.get(5)));
        }
        else{
            throw new MyException("Document \""+objectType+"\" does not exists.");
        }

    }
}
