package Compulsory;

import Optional.Combination;
import org.springframework.context.access.ContextJndiBeanFactoryLocator;

import java.util.*;

public abstract class Player implements Runnable {
    private String name;
    private Game game;
    private List<Character> myLetters;
    private int points;

    public List<Character> getMyLetters() {
        return myLetters;
    }

    public void setMyLetters(List<Character> myLetters) {
        this.myLetters = myLetters;
    }

    public Player(String name) {
        this.name = name;
        this.points = 0;
        myLetters = new ArrayList<Character>();
    }

    private synchronized boolean createSubmitWord() throws InterruptedException {

        if(myLetters.size() != 7){
                myLetters.addAll(game.getBag().extractLetters(7 - myLetters.size()));
        }

        if (myLetters.isEmpty()) {
            return false;
        }
        StringBuilder word = new StringBuilder();
        Random random = new Random();
        for (int i = myLetters.size(); i > 0; i--) {
            Combination coomb = new Combination(this,i);
            List<String> validWords = coomb.getValidWords();

            if(!validWords.isEmpty())
            {
                int randomNum = random.nextInt(validWords.size());
                String string = validWords.get(randomNum);
                for(char c: string.toCharArray()){
                    word.append(c);
                    myLetters.remove((Character)c);
                }

                points = points + word.toString().length() * 10;
                game.getBoard().addWord(this, word.toString());
                Thread.sleep(300);
                return true;

            }

        }
        Thread.sleep(300);
        return true;
    }


    @Override
    public void run() {
        while(game.getBag().getLetters().size() != 0) {
            try {
                createSubmitWord();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }




    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", game=" + game +
                ", myLetters=" + myLetters +
                ", points=" + points +
                '}';
    }

    public synchronized List<String> getValidWords(List<Character> characterList,int k){
        Object[] array = characterList.toArray(new Object[characterList.size()]);
        List<String> combinations = new ArrayList<String>();
        combination(combinations, array,k);
        List<String> common = new ArrayList<String>();
        for(String string: combinations){
            if(game.words.containsWord(string)){
                common.add(string);
            }
        }

        System.out.println(this.name+ " " + characterList + " " + common);
        return common;

    }

    public synchronized void combination(List<String> combinations, Object[]  elements, int K){

        // get the length of the array
        // e.g. for {'A','B','C','D'} => N = 4
        int N = elements.length;

        if(K > N){
            System.out.println("Invalid input, K > N");
            return;
        }
        // calculate the possible combinations
        // e.g. c(4,2)
        c(N,K);

        // get the combination by index
        // e.g. 01 --> AB , 23 --> CD
        int combination[] = new int[K];

        // position of current index
        //  if (r = 1)				r*
        //	index ==>		0	|	1	|	2
        //	element ==>		A	|	B	|	C
        int r = 0;
        int index = 0;

        while(r >= 0){
            // possible indexes for 1st position "r=0" are "0,1,2" --> "A,B,C"
            // possible indexes for 2nd position "r=1" are "1,2,3" --> "B,C,D"

            // for r = 0 ==> index < (4+ (0 - 2)) = 2
            if(index <= (N + (r - K))){
                combination[r] = index;

                // if we are at the last position print and increase the index
                if(r == K-1){

                    //do something with the combination e.g. add to list or print
                    print(combinations, combination, elements);
                    index++;
                }
                else{
                    // select index for next position
                    index = combination[r]+1;
                    r++;
                }
            }
            else{
                r--;
                if(r > 0)
                    index = combination[r]+1;
                else
                    index = combination[0]+1;
            }
        }
    }

    public synchronized int c(int n, int r){
        int nf=fact(n);
        int rf=fact(r);
        int nrf=fact(n-r);
        int npr=nf/nrf;
        int ncr=npr/rf;
        return ncr;
    }

    public synchronized int fact(int n)
    {
        if(n == 0)
            return 1;
        else
            return n * fact(n-1);
    }
    public synchronized void print(List<String> combinations, int[] combination, Object[] elements){

        String output = "";
        for(int z = 0 ; z < combination.length;z++) {
            output += elements[combination[z]];
        }
        combinations.add(output);
    }


}