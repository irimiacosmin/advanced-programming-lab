package com.java.ro;

import java.util.function.Consumer;

public class Main {

    public static void main(String argv[])
    {
        exOne();
    }

    public static void exOne(){

        Building h1 = new Building("House 1", 27, 9); // 9
        Building h2 = new Building("House 2", 27, 9); // 3
        Building h3 = new Building("House 3", 64, 16);// 4


        Vehicle c1 = new Vehicle("Car 1", 8, 4);// 2
        Vehicle c2 = new Vehicle("Car 2", 8, 4);// 2

        Jewel ring = new Jewel("Gold Diamond Ring",2);


        AssetManager manager = new AssetManager();
        manager.add(h1, h2, h3);
        manager.add(c1, c2);
        manager.add(ring);

        System.out.println("Items sorted by name: " + manager.getItems());
        System.out.println("Assets sorted by profit: " + manager.getAssets());

        int maxValue = 20;
        Portofolio solution = manager.createPortofolio(new DynamicAlgorithm(), maxValue);
        System.out.println("The best portofolio: " + solution);
    }

}

