package classes;

import java.io.Serializable;
import java.util.List;

public class Chart implements Serializable {

    List<Album> albumList;

    public Chart(){

    }

    public Chart(List<Album> albumList) {
        this.albumList = albumList;
    }


    public List<Album> getAlbumList() {
        return albumList;
    }

    public void setAlbumList(List<Album> albumList) {
        this.albumList = albumList;
    }

    @Override
    public String toString() {
        return "Chart{" +
                "albumList=" + albumList +
                '}';
    }
}
