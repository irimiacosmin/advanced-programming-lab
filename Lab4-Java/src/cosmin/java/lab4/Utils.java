package cosmin.java.lab4;

import java.awt.*;
import java.beans.XMLEncoder;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;

public class Utils {

    public static void serializare(String path, Catalog catalog) {

        try {
            FileOutputStream fos = new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(catalog);
            out.flush();
            fos.close();
        }catch (Exception exception){
            exception.printStackTrace();
        }

    }

    public static Catalog deserializare(String path) {

        Catalog catalog = null;
        try {
            FileInputStream fis = new FileInputStream(path);
            ObjectInputStream in = new ObjectInputStream(fis);
            catalog = (Catalog) in.readObject();
            fis.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return catalog;
    }

    public static void openFile(String path){

        try {
            ProcessBuilder processBuilder = new ProcessBuilder("Notepad.exe", path);
            processBuilder.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static boolean openWebpage(String browser,String url) {
        try {
            URI myURI = new URI("https://www."+browser+".com/search?q="+url.replace(" ","%20"));
            return openWebpage(myURI);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return false;
    }


    public static void writeXML(Object f, String filename) throws Exception{
        XMLEncoder encoder =
                new XMLEncoder(
                        new BufferedOutputStream(
                                new FileOutputStream(filename)));
        encoder.writeObject(f);
        encoder.close();
    }




}
