package Compulsory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class GameClient {
    private final static String SERVER_ADDRESS = "127.0.0.1";
    private final static int PORT = 8100;
    private static Socket socket = null;
    private static boolean ready = false;
    public static void main(String[] args) throws IOException {
        System.out.println("Client started!");
        System.out.println("You can use commands: <create [MAX_NUMBER]> or <submit [NUMBER]> or <quit>");
        socket = new Socket(SERVER_ADDRESS,PORT);
        GameClient client = new GameClient();
        while (true) {
            String request = client.readFromKeyboard();
            if (request.equalsIgnoreCase("exit")) {
                sendExit();
                break;
            } else if (request.equalsIgnoreCase("quit")) {
                client.sendRequestToServer(request);
                break;
            }
            else {
                client.sendRequestToServer(request);
                if(ready==true){
                    break;
                }
            }
        }
    }
    public static void sendExit() throws IOException {
        PrintWriter out = new PrintWriter(socket.getOutputStream());
        out.println("exit");
        out.flush();
        socket.close();
    }

    public void sendRequestToServer(String request) throws IOException {


        PrintWriter out = new PrintWriter(socket.getOutputStream());
        out.println(request);
        out.flush();

        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String response = in.readLine();
        if(response.endsWith("$")){
            System.out.println(response.substring(0,response.length()-1));
            socket.close();
            ready = true;
        }else{
            System.out.println(response);
        }
    }

    private String readFromKeyboard() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
