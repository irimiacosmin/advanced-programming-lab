package com.java.ro;

import java.util.ArrayList;
import java.util.List;

public class DynamicAlgorithm implements Algorithm{

    public List<Item> computeSolution(List<Item> assetList, int maxValue){

        List<Item> bestPortofolio = new ArrayList<Item>();
        int size = assetList.size();

        List<Item> items = Utils.sortByPriceDesc(assetList);

        int[] profit = new int[size+1];//  2 2 3 3  7
        int[] price = new int[size+1]; //  4 4 9 9 16

        for(int j=0; j< size; j++)
        {
            profit[j] = (int) ((Asset) assetList.get(j)).computeProfit();
            price[j] = (int) ((Item)  assetList.get(j)).getPrice();
        }

        //creates array that will contain the optimal solution as well as the solution to the sub problems
        int[][] array = new int[items.size()][maxValue+1];
        //creates an array containing the items that are in the optimal solution
        int[][] itemsToKeep = new int[items.size()][maxValue+1];

        //sets all items of item number 0 to 0 for all weights up to the max weight
        array[0][0] = 0;
        for(int a = 0; a < maxValue; a++) {
            array[0][a] = 0;
        }
        //loops through the item numbers
        for(int i = 1; i < array.length; i++)
        {
            //loops through the item weights - finds the optimal solution for the given item and the given weight in the array
            for(int j = 1; j < array[i].length; j++)
            {
                if(price[i] <= j && profit[i] + array[i-1][j-price[i]] >= array[i-1][j])
                {
                    array[i][j] = (profit[i] + array[i-1][j-price[i]]);
                    itemsToKeep[i][j] = 1;
                }
                else {
                    array[i][j] = array[i-1][j];
                    itemsToKeep[i][j] = 0;
                }
            }
        }
        int weight = maxValue;
        int counter = 0;
        //Determines which items were kept in the optimal solution of the knapsack problem
        for(int i = items.size()-1; i >= 0 ; i--) {
            if(itemsToKeep[i][weight] == 1)
            {
                bestPortofolio.add(items.get(i));
                weight-= price[i];
                counter += price[i];
            }
        }
        return bestPortofolio;
    }












}
