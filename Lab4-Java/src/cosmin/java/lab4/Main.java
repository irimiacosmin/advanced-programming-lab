package cosmin.java.lab4;

import cosmin.java.commands.*;
import cosmin.java.userInterface.CatalogFrame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main{

    public static void main(String argv[]) throws Exception {


        Catalog catalog = new Catalog();

        catalog.add (new Book("The Art of Computer Programming", "d:\\books\\programming\\tacp.ps", 1967, "Donald E. Knuth"));
        catalog.add (new Article("Mastering the Game of Go without Human Knowledge", "d:\\articles\\AlphaGo.pdf", 2017, "David Silver", "Julian Schrittwieser", "Karen Simonyan"));
        catalog.add (new Manual("Cookbook", "d:\\stuff\\cookbook.doc",2016,"Tim Cook"));

        //catalog.save(".\\catalog.dat");
        //catalog.load(".\\catalog.dat");
        //catalog.open(".\\catalog.dat");
        //catalog.list();

        //shell(catalog);

        CatalogFrame catalogFrame = new CatalogFrame();
        catalogFrame.setVisible(true);

    }

    public static void shell(Catalog catalog) throws MyException, IOException {

        Scanner keyboard = new Scanner(System.in);
        boolean ok=true;
        String text,comand;
        List<String> parts;
        while(ok==true){

            System.out.print("\n> ");
            text = keyboard.nextLine();
            parts = new ArrayList<String>();
            Matcher m = Pattern.compile("([^\"]\\S*|\".+?\")\\s*").matcher(text);
            while (m.find())
            {
                parts.add( m.group(1).replace("\"", ""));
            }

            comand = parts.get(0);
            if(comand.equals("add")){
                new AddCommand(catalog,parts);
            }
            else if(comand.equals("list")){
                new ListCommand(catalog,parts);
            }
            else if(comand.equals("save")){
                new SaveCommand(catalog,parts);
            }
            else if(comand.equals("load")){
                new LoadCommand(catalog,parts);
            }
            else if(comand.equals("play")) {
                new PlayCommand(catalog,parts);
            }
            else if(comand.equals("quit") || comand.equals("exit")) {
                ok=false;
            }
            else if(comand.equals("info")){
                new InfoCommand(parts,catalog);
            }
            else if(comand.equals("report")){
                new ReportCommand(parts,catalog);
            }
            else{
                throw new MyException("Comand \""+comand+"\" does not exists.");
            }

        }

    }


}
