package cosmin.java.commands;

import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.MyException;

import java.util.List;

public class SaveCommand  implements ShellFace {

    public List<String> arguments;
    public Catalog catalog;


    public SaveCommand(Catalog catalog, List<String> arguments) throws MyException {
        this.arguments = arguments;
        this.catalog = catalog;

        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException {
        if(arguments.size() < 1)
        {
            throw new MyException("Too few arguments.");
        }

        catalog.save(arguments.get(1));
    }
}
