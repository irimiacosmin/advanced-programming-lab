package models;

import classes.Chart;

import javax.persistence.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "Charts", schema = "MusicAlbums", catalog = "")
public class ChartsEntity {
    private byte[] chart;
    private int id;

    @Basic
    @Column(name = "chart")
    public byte[] getChart() {
        return chart;
    }

    public void setChart(byte[] chart) {
        this.chart = chart;
    }

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChartsEntity that = (ChartsEntity) o;
        return id == that.id &&
                Arrays.equals(chart, that.chart);
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(id);
        result = 31 * result + Arrays.hashCode(chart);
        return result;
    }

    public Chart readData() throws SQLException, IOException, ClassNotFoundException {
        byte[] buf = this.getChart();
        if (buf != null) {
            ObjectInputStream objectIn = new ObjectInputStream(
                    new ByteArrayInputStream(buf));
            return (Chart) objectIn.readObject();
        }
        return null;
    }
}
