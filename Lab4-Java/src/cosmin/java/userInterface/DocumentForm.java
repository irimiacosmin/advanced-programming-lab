package cosmin.java.userInterface;

import cosmin.java.lab4.Document;
import cosmin.java.lab4.MyException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DocumentForm extends JPanel {
    private final CatalogFrame frame;
    JLabel titleLabel = new JLabel("Title of the document");
    JLabel pathLabel = new JLabel("Path in the local file system");
    JLabel yearLabel = new JLabel("Publication Year");

    JTextField titleTextField = new JTextField();
    JTextField pathTextField = new JTextField();

    JButton addButton = new JButton("Add to repository");

    JSpinner yearField = new JSpinner(new SpinnerNumberModel(2010, 1900, 2017, 1));

    public DocumentForm(CatalogFrame frame){
        this.frame = frame;
        init();
    }


    private void init() {

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        setBorder(BorderFactory.createEmptyBorder(10, 50, 10, 50));
        yearField.setMaximumSize(new Dimension(200,10));
        addButton.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        setAlignmentX(Component.RIGHT_ALIGNMENT);

        add(titleLabel);
        add(titleTextField);
        add(pathLabel);
        add(pathTextField);
        add(yearLabel);
        add(yearField);
        add(addButton);
        addButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String title,path,year;
                title = titleTextField.getText();
                path = pathTextField.getText();
                year = yearField.getValue().toString();
                addDocument(title + ", "+year + " ["+path+"]");
                try {
                    CatalogFrame.catalog.add(new Document(title,path,Integer.parseInt(year),"unknown"));
                } catch (MyException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }
    public void addDocument(String item) {
        frame.list.addDocument(item);
    }


}
