package com.java.ro;

public class Building extends Item implements Asset{

    double area;

    public Building(String name, double area, double price) {
        super(name,price);
        this.area = area;

    }

    @Override
    public double computeProfit() {
        return area / price;
    }

    @Override
    public String toString() {
        return "Building{" +
                "area=" + area +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
