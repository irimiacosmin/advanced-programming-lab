package com.java.ro;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AssetManager {

    List<Item> itemList;

    public AssetManager() {
        itemList = new ArrayList<Item>();
    }

    public void add(Item ... items){
        for(Item item: items)
            itemList.add(item);
    }

    public List<Item> getItems(){

        Collections.sort(itemList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        return itemList;
    }

    public List<Item> getAssets(){

        List<Item> assetList = new ArrayList<Item>();
        for(Item item: itemList)
            if(item instanceof Asset)
                assetList.add(item);

       assetList = Utils.sortByProfit(assetList);

        return assetList;
    }

    public Portofolio createPortofolio(Algorithm algorithm, int maxValue){
        return new Portofolio(algorithm.computeSolution(getAssets(),maxValue));
    }

}
