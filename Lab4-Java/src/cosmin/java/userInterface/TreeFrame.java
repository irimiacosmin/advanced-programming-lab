package cosmin.java.userInterface;

import cosmin.java.commands.ReportCommand;
import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.Document;
import cosmin.java.lab4.MyException;
import cosmin.java.lab4.Utils;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class TreeFrame extends JFrame {

    private JTree tree;
    public Catalog catalog;
    public int isLeaf;
    public CatalogFrame frame;
    public DefaultMutableTreeNode node;
    public DefaultMutableTreeNode root;
    public final DocumentForm documentForm;
    public ControlPanel controlPanel;

    public TreeFrame(Catalog catalog, DocumentForm documentForm, CatalogFrame frame, ControlPanel controlPanel)
    {
        this.catalog = catalog;
        this.documentForm = documentForm;
        this.frame = frame;
        this.controlPanel = controlPanel;

        root = createTree();
        tree = new JTree(root);
        tree.setEditable(true);
        add(tree);


        JMenuBar menubar = new JMenuBar();
        JMenu newMenu = new JMenu("Nodes");
        JMenu openMenu = new JMenu("Open");
        JMenu searchMenu = new JMenu("Search");
        JMenu reportMenu = new JMenu("Report");

        newMenu.setMnemonic(KeyEvent.VK_F);
        openMenu.setMnemonic(KeyEvent.VK_F);
        searchMenu.setMnemonic(KeyEvent.VK_F);

        JMenuItem eMenuItem = new JMenuItem("New node");

        eMenuItem.setMnemonic(KeyEvent.VK_E);
        eMenuItem.addActionListener((ActionEvent event) -> {
            display();
        });

        JMenuItem eMenuItem6 = new JMenuItem("Load Catalog");
        eMenuItem6.setMnemonic(KeyEvent.VK_E);
        eMenuItem6.addActionListener((ActionEvent event) -> {

            JFileChooser jfc = new JFileChooser(".//");
            jfc.setDialogTitle("Select a catalog to load ...");
            jfc.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filter = new FileNameExtensionFilter(".dat", "dat");
            jfc.addChoosableFileFilter(filter);

            int returnValue = jfc.showOpenDialog(null);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                frame.list.removeAll();
                CatalogFrame.catalog = new Catalog();
                CatalogFrame.catalog.load(jfc.getSelectedFile().getPath());
                for(Document document: CatalogFrame.catalog.getDocumentList())
                    frame.list.addDocument(document.title+", "+String.valueOf(document.year)+" ["+document.path+"]");
            }

            setVisible(false);
            dispose();

            controlPanel.callBeibi();

        });


        JMenuItem eMenuItem7 = new JMenuItem("Save Catalog");
        eMenuItem7.setMnemonic(KeyEvent.VK_E);
        eMenuItem7.addActionListener((ActionEvent event) -> {

            String response = JOptionPane.showInputDialog(null,
                    "Please insert filename",
                    "Filename",
                    JOptionPane.QUESTION_MESSAGE);

            if(response.equals(null) || response.equals("") || response.equals(" ")){
                JOptionPane.showMessageDialog(frame, "Filename cannot be null!");
            }
            else {
                String filename = response.replaceAll("\\s+","");
                CatalogFrame.catalog.save(filename+".dat");
            }

        });

        newMenu.add(eMenuItem);
        newMenu.add(eMenuItem6);
        newMenu.add(eMenuItem7);

        JMenuItem eMenuItem2 = new JMenuItem("Current node");
        eMenuItem2.setMnemonic(KeyEvent.VK_E);
        eMenuItem2.addActionListener((ActionEvent event) -> {
            if(node.isLeaf()){
                Utils.openFile(node.getParent().getChildAt(0).toString());
            }
            else if(node.equals(root)){
                JOptionPane.showMessageDialog(this, "Catalog is not openable.");

            }
            else{
                Utils.openFile(node.getFirstLeaf().toString());
            }


        });

        openMenu.add(eMenuItem2);

        JMenuItem eMenuItem3 = new JMenuItem("Google");
        eMenuItem3.setMnemonic(KeyEvent.VK_E);
        eMenuItem3.addActionListener((ActionEvent event) -> {

            if(node.isLeaf()){
                Utils.openWebpage("google",node.getParent().toString());
            }
            else if(node.equals(root)){
                JOptionPane.showMessageDialog(this, "Catalog is not searchable.");

            }
            else{
                Utils.openWebpage("google",node.toString());
            }

        });

        JMenuItem eMenuItem4 = new JMenuItem("Yahoo");
        eMenuItem4.setMnemonic(KeyEvent.VK_E);
        eMenuItem4.addActionListener((ActionEvent event) -> {

            if(node.isLeaf()){
                Utils.openWebpage("yahoo",node.getParent().toString());
            }
            else if(node.equals(root)){
                JOptionPane.showMessageDialog(this, "Catalog is not searchable.");

            }
            else{
                Utils.openWebpage("yahoo",node.toString());
            }

        });

        JMenuItem eMenuItem5 = new JMenuItem("Bing");
        eMenuItem5.setMnemonic(KeyEvent.VK_E);
        eMenuItem5.addActionListener((ActionEvent event) -> {
                Utils.openWebpage("bing","i am retarded please help");
        });


        JMenuItem repo1 = new JMenuItem("HTML");
        repo1.setMnemonic(KeyEvent.VK_E);
        repo1.addActionListener((ActionEvent event) -> {
            startReport("html");
        });

        JMenuItem repo2 = new JMenuItem("DOC");
        repo2.setMnemonic(KeyEvent.VK_E);
        repo2.addActionListener((ActionEvent event) -> {
            startReport("doc");
        });

        JMenuItem repo3 = new JMenuItem("TXT");
        repo3.setMnemonic(KeyEvent.VK_E);
        repo3.addActionListener((ActionEvent event) -> {
            startReport("txt");
        });

        JMenuItem repo4 = new JMenuItem("XLS");
        repo4.setMnemonic(KeyEvent.VK_E);
        repo4.addActionListener((ActionEvent event) -> {
            startReport("xls");
        });

        reportMenu.add(repo1);
        reportMenu.add(repo2);
        reportMenu.add(repo3);
        reportMenu.add(repo4);


        searchMenu.add(eMenuItem3);
        searchMenu.add(eMenuItem4);
        searchMenu.add(eMenuItem5);

        menubar.add(newMenu);
        menubar.add(openMenu);
        menubar.add(searchMenu);
        menubar.add(reportMenu);
        setJMenuBar(menubar);

        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();

                if (node == null)
                    isLeaf = 1;

                if (node.isLeaf()) {
                    isLeaf = 1;
                } else {
                    isLeaf = 0;
                }

            }
        });

        this.setTitle("JTree Example");
        this.pack();
        this.setSize(new Dimension(600,500));
        this.setVisible(true);

    }

    public DefaultMutableTreeNode createTree(){

        DefaultMutableTreeNode root = new DefaultMutableTreeNode("Catalog");
        DefaultMutableTreeNode title,path,year;

        for(Document document: catalog.getDocumentList())
        {
            title = new DefaultMutableTreeNode(document.title);
            path = new DefaultMutableTreeNode(document.path);
            year = new DefaultMutableTreeNode(String.valueOf(document.year));
            title.add(path);
            title.add(year);
            root.add(title);
        }

        return root;

    }

    public void addNode(String title1, String path1, String year1){

        DefaultMutableTreeNode title,path,year;
        title = new DefaultMutableTreeNode(title1);
        path = new DefaultMutableTreeNode(path1);
        year = new DefaultMutableTreeNode(year1);
        title.add(path);
        title.add(year);

        DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
        DefaultMutableTreeNode notRoot = (DefaultMutableTreeNode) model.getRoot();
        model.insertNodeInto(title, notRoot, notRoot.getChildCount());

        documentForm.addDocument(title1 + ", "+year1 + " ["+path1+"]");
        try {
            CatalogFrame.catalog.add(new Document(title1,path1,Integer.parseInt(year1)," "));
        } catch (MyException e1) {
            e1.printStackTrace();
        }
        CatalogFrame.catalog.list();

    }




    private void display() {
        JTextField field1 = new JTextField("");
        JTextField field2 = new JTextField("");
        JTextField field3 = new JTextField("");
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Title"));
        panel.add(field1);
        panel.add(new JLabel("Path"));
        panel.add(field2);
        panel.add(new JLabel("Year"));
        panel.add(field3);
        int result = JOptionPane.showConfirmDialog(null, panel, "Add a new document ... ",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {

            String regularExpression = "([A-Z|a-z]:\\\\[^*|\"<>?\\n]*)|(\\\\\\\\.*?\\\\.*)";
            Pattern pattern = Pattern.compile(regularExpression);
            boolean isMatched = pattern.matches(regularExpression,field2.getText());

            if(field1.getText().replace(" ", "").equals("") ||
                    field2.getText().replace(" ", "").equals("") ||
                    field3.getText().replace(" ", "").equals("") ||
                    field2.getText().contains(" ") || field3.getText().length()!=4 || field1.getText().length()<3
                    || field2.getText().length()<3 || isMatched==false)
                JOptionPane.showMessageDialog(this, "Input is not valid!");
            else {
                addNode(field1.getText(),field2.getText(),field3.getText());
            }

        } else {
            System.out.println("Cancelled");
        }
    }

    public void startReport(String extension)
    {
        List<String> arguments = new ArrayList<>();
        arguments.add("report");
        arguments.add(extension.toLowerCase());
        try {

            new ReportCommand(arguments,CatalogFrame.catalog);
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (MyException e1) {
            e1.printStackTrace();
        }
    }


}
