public interface Person {


    String getStudentID();
    String getTeacherID();
    String getName();
    String getSurname();
    String getEmail();
    Project createProject(String name,int capacity);

}
