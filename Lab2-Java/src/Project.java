import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Project {

    private String name;
    private int capacity;
    private int lastPostion;
    private int nrStudsAllocated;
    private List<Student> preferences;

    public Project(String name, int capacity) {
        this.name = name;
        this.capacity = capacity;
        this.lastPostion = capacity;
        this.nrStudsAllocated = 0;
        preferences = new ArrayList<Student>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }


    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }


    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", capacity=" + capacity +
                '}';
    }

    public int getLastPostion() {
        if(nrStudsAllocated < capacity)
            return preferences.size();

        for(int i = lastPostion; i >= 0; i--){
            if(preferences.get(i).getPositionInProjectList() > -1 &&
                    preferences.get(i).getProject().equals(this))
                return i;
        }
        return -1;
    }

    public void setLastPostion(int lastPostion) {
        this.lastPostion = lastPostion;
    }

    public int getNrStudsAllocated() {
        return nrStudsAllocated;
    }

    public void incrementNrStudsAllocated(){
        this.nrStudsAllocated++;
    }

    public void setNrStudsAllocated(int nrStudsAllocated) {
        this.nrStudsAllocated = nrStudsAllocated;
    }

    public List<Student> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Student> preferences) {
        this.preferences = preferences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return capacity == project.capacity &&
                lastPostion == project.lastPostion &&
                nrStudsAllocated == project.nrStudsAllocated &&
                Objects.equals(name, project.name) &&
                Objects.equals(preferences, project.preferences);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, capacity, lastPostion, nrStudsAllocated, preferences);
    }


    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
