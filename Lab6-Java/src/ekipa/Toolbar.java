package ekipa;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Toolbar extends JPanel {

    JLabel sidesLabel = new JLabel("Number of slides:");
    JLabel shapesLabel = new JLabel("Number of shapes:");
    JLabel sizeLabel = new JLabel("Size of shapes:");
    JLabel stokeLabel = new JLabel("Stroke");
    JSpinner sidesNo = new JSpinner(new SpinnerNumberModel(3, 3, 10, 1));
    JSpinner shapesNo = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
    JSpinner shapesSize = new JSpinner(new SpinnerNumberModel(40, 10, 100, 10));
    String[] strokes = { "Fill", "Line", "Dashed"};
    JComboBox strokeList = new JComboBox(strokes);
    JButton drawBtn = new JButton("Draw");
    JCheckBox transparentCB = new JCheckBox("Transparent");

    Canvas canvas;
    public Toolbar(Canvas canvas) {
        this.canvas = canvas;
        add(sizeLabel);
        add(shapesSize);
        add(sidesLabel);
        add(sidesNo);
        add(shapesLabel);
        add(shapesNo);
        add(stokeLabel);
        add(strokeList);
        add(drawBtn);
        add(transparentCB);

        drawBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                int noOfSh = Integer.parseInt(shapesNo.getValue().toString());
                int noOfSd = Integer.parseInt(sidesNo.getValue().toString());
                int size = Integer.parseInt(shapesSize.getValue().toString());
                int stroke = strokeList.getSelectedIndex();
                boolean isTrans = transparentCB.isSelected();
                Random rand = new Random();


                for(int i = 1; i <= noOfSh; i++){

                    int resultX = rand.nextInt(800-size) + 10 + size;
                    int resultY = rand.nextInt(500-size) + 10 + size;
                    canvas.drawShapeAt(resultX,resultY,size,noOfSd,stroke+1,isTrans);
                }
            }
        });

    }
}
