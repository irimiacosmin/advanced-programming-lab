package Optional;

public interface Dictionary {
    boolean containsWord(String str);
}
