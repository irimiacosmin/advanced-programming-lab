package ekipa;

import functions.FunctionPanel;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class ControlPanel extends JPanel {

    public Canvas canvas;
    public ControlPanel controlPanel;
    public JButton loadBtn = new JButton("Load");
    public JButton saveBtn = new JButton("Save");
    public JButton resetBtn = new JButton("Reset");
    public JButton functionBtn = new JButton("Functions");

    public ControlPanel(Canvas canvas) {
        this.canvas = canvas;
        this.controlPanel = this;
        add(loadBtn);
        add(saveBtn);
        add(resetBtn);
        add(functionBtn);

        loadBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser jfc = new JFileChooser(".//");
                jfc.setDialogTitle("Select a graphic photo to load ...");
                jfc.setAcceptAllFileFilterUsed(false);
                String[] types = {"png","jpg","bmp","jpeg","PNG","JPG","BMP","JPEG"};
                FileNameExtensionFilter filter = new FileNameExtensionFilter("images", types);
                jfc.addChoosableFileFilter(filter);
                int returnValue = jfc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    load(jfc.getSelectedFile().getPath());
                }
            }
        });

        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               display();
            }
        });

        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                canvas.bufferedImageArrayList = new ArrayList<BufferedImage>();
                canvas.img = null;
            }
        });

        functionBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new FunctionPanel();
            }
        });
    }

    public void load(String filename){
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(filename));
            canvas.img = img;
        } catch (IOException e) {


        }
    }

    public void save(String filename,String extension)
    {
        BufferedImage bImg = new BufferedImage(canvas.getWidth(), canvas.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D cg = bImg.createGraphics();
        canvas.paintAll(cg);
        try {
            ImageIO.write(bImg, extension, new File(filename+"."+extension));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void display() {
        JTextField field1 = new JTextField("");
        String[] ext = { "png", "jpeg", "bmp"};

        JComboBox extList = new JComboBox(ext);

        JPanel panel = new JPanel(new GridLayout(2, 1));
        panel.add(new JLabel("Filename:"));
        panel.add(field1);

        panel.add(new JLabel("Extension:"));
        panel.add(extList);

        int result = JOptionPane.showConfirmDialog(null, panel, "Filename", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {

            String filename = field1.getText().replaceAll("\\s+","");

            if(filename.equals("") || filename.equals(" "))
                JOptionPane.showMessageDialog(this, "Filename is not valid!");
            else {
                save(filename,ext[extList.getSelectedIndex()]);
            }

        } else {
            System.out.println("Cancelled");
        }
    }

}

