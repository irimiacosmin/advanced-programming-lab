package cosmin.java.commands;

import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.Document;
import cosmin.java.lab4.MyException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class ReportCommand implements ShellFace  {

    public List<String> arguments;
    public Catalog catalog;

    public ReportCommand(Catalog catalog){
        this.catalog = catalog;
    }

    public ReportCommand(List<String> arguments, Catalog catalog) throws IOException, MyException {
        this.arguments = arguments;
        this.catalog = catalog;
        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException, IOException {

        String objectType = arguments.get(1);
        if(arguments.size() < 2)
        {
            throw new MyException("Too few arguments.");
        }

        if(objectType.toLowerCase().equals("html")){
            makeHTML();
        }else if(objectType.toLowerCase().equals("pdf")){
            makePDF();
        }else if(objectType.toLowerCase().equals("xls")){
            makeXLS();
        }else if(objectType.toLowerCase().equals("doc")){
            makeDOC();
        } else if(objectType.toLowerCase().equals("txt")){
            makeTXT();
        } else{
            throw new MyException("Format is not supported.");
        }

    }

    public void makeHTML() throws IOException {
        FileWriter fileWriter = new FileWriter("report.html");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<head>\n" +
                "\t\t<meta charset=\"utf-8\">\n" +
                "\t\t<title>Laborator 4 - JAVA</title>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<h3>Laborator 4 - JAVA</h3>\n"+
                "\t\t<ul>\n" +
                "\t\t\t<li>Catalog</li>\n" +
                "\t\t\t<ol>\n";

        for(Document document : catalog.getDocumentList()){
            header = header + itemToHTML(document.title,document.path,String.valueOf(document.year),document.authors);

        }

        String footer = "\n</ol>\t\t\t\t\t\n" +
                "\t\t</ul>\t\t\n" +
                "\t</body>\n" +
                "</html>\t";


        bufferedWriter.write(header+footer);
        bufferedWriter.close();
        fileWriter.close();

    }

    public String itemToHTML(String title, String path, String year, List<String> authors){

        String model = "\n\t\t\t\t<li>"+title+"</li>\n" +
                "\t\t\t\t<ol>\n" +
                "\t\t\t\t\t<li>"+path+"</li>\n" +
                "\t\t\t\t\t<li>"+year+"</li>\n" +
                "\t\t\t\t\t<li>Autori</li>\n" +
                "\t\t\t\t\t<ol>\n";

        for(String autor : authors)
            model = model + "\t\t\t\t\t\t<li>"+autor+"</li>\n";

        return model + "\t\t\t\t\t</ol>\n" + "\t\t\t\t</ol>";

    }

    public void makePDF() throws IOException  {
        makeFile("report.pdf");
    }

    public void makeXLS() throws IOException {
        makeFile("report.xls");
    }

    public void makeDOC() throws IOException {
        makeFile("report.doc");
    }

    public void makeTXT() throws IOException {
        makeFile("report.txt");
    }

    public void makeFile(String filename) throws IOException {

        FileWriter fileWriter = new FileWriter(filename);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

        String header = "Laborator 4 - JAVA\n" + "\n" + "Catalog\n";

        int it = 1;
        for(Document document : catalog.getDocumentList()){
            header = header + itemToTXT(it,document.title,document.path,String.valueOf(document.year),document.authors);
            it = it+1;
        }

        bufferedWriter.write(header);
        bufferedWriter.close();
        fileWriter.close();
    }

    public String itemToTXT(int it, String title, String path, String year, List<String> authors){


        String model = "\t"+it+"."+title+"\n" +
                "\t\t1."+path+"\n" +
                "\t\t2."+year+"\n" +
                "\t\t3.Authors:\n";
        int i = 1;
        for(String autor : authors) {
            model = model + "\t\t\t"+i+"."+autor+"\n";
            i = i+1;
        }
        return model;


    }


}
