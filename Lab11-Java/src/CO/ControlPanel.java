package CO;

import custom.button.CosminButton;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class ControlPanel extends JPanel implements Serializable{
    private final MainFrame frame;
    private final JLabel classNameLabel = new JLabel("Class name");
    private final JTextField classNameField = new JTextField(30);
    private final JLabel textLabel = new JLabel("Default text");
    private final JTextField textField = new JTextField(10);
    private final JButton createButton = new JButton("Add component");
    private final JButton saveButton = new JButton("Save");
    private final JButton loadButton = new JButton("Load");
    private ArrayList<JFrame> tables = null;
    public ControlPanel(MainFrame frame) {
        this.frame = frame;
        tables = new ArrayList<JFrame>();
        init();
    }
    private void init() {
        add(classNameLabel); add(classNameField); add(textLabel); add(textField); add(createButton); add(saveButton);add(loadButton);
        createButton.addActionListener(e -> {
                JComponent comp = null;
                try {
                    comp = createDynamicComponent(classNameField.getText());
                } catch (ClassNotFoundException e1) {
                    e1.printStackTrace();
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (InstantiationException e1) {
                    e1.printStackTrace();
                }
                setComponentText(comp, textField.getText());
                frame.designPanel.addAtRandomLocation(comp);

        });

        saveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    saveDesignPanel();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        loadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                JFileChooser jfc = new JFileChooser(".//");
                jfc.setDialogTitle("Select a .file to load ...");
                jfc.setAcceptAllFileFilterUsed(false);
                String[] types = {"file"};
                FileNameExtensionFilter filter = new FileNameExtensionFilter("files", types);
                jfc.addChoosableFileFilter(filter);
                int returnValue = jfc.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    try {
                        loadDesignPanel(jfc.getSelectedFile().getPath());
                        frame.repaint();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    } catch (ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }


            }
        });

    }
    private JComponent createDynamicComponent(String className) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        JComponent component = null;
        try{
            component = (JComponent) Class.forName(className).newInstance();
            JFrame table = tableShow(component);
            component.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseEntered(MouseEvent e) {
                    for(JFrame jp : tables){
                        jp.setVisible(false);
                    }
                    tables.add(table);
                    table.setVisible(true);
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    table.setVisible(false);
                }

            });
            myIntrospector(component);
        }catch (Exception e){
            e.printStackTrace();
        }
        return component;
    }

    private void saveDesignPanel() throws IOException {

        FileOutputStream fileOut = new FileOutputStream("CO.DesignPanel.file");
        ObjectOutputStream out = new ObjectOutputStream(fileOut);
        out.writeObject(frame.designPanel);
        out.close();
        fileOut.close();

    }

    private void loadDesignPanel(String file) throws IOException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream inputStream = new ObjectInputStream(fileInputStream);
        DesignPanel designPanel = (DesignPanel) inputStream.readObject();
        if(designPanel!=null){
            frame.designPanel = designPanel;
        }
        inputStream.close();
        fileInputStream.close();
        System.out.println(designPanel);
    }

    private void setComponentText(JComponent comp, String text) {

        if(text.equals("") || text.equals(" ") || text.equals(null)){
            text = "Test";
        }

        if(comp instanceof JLabel){
            JLabel label = (JLabel) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof JButton){
            JButton label = (JButton) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof JTextField){
            JTextField label = (JTextField) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof JPasswordField){
            JPasswordField label = (JPasswordField) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof JTextArea){
            JTextArea label = (JTextArea) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof JCheckBox){
            JCheckBox label = (JCheckBox) comp;
            label.setText(text);
            comp = (JComponent) label;
        }else if(comp instanceof CosminButton){
            CosminButton label = (CosminButton) comp;
            label.setText(text);
            comp = (JComponent) label;
        }
        int marime = text.length()*5 + 50;
        comp.setPreferredSize(new Dimension(marime,30));
    }

    private PropertyDescriptor[] myIntrospector(JComponent jComponent) throws IntrospectionException {

        BeanInfo beanInfo = Introspector.getBeanInfo(jComponent.getClass());
        PropertyDescriptor[] descriptors = beanInfo.getPropertyDescriptors();
        return descriptors;


    }

    public JFrame tableShow(JComponent component) throws IntrospectionException, InvocationTargetException, IllegalAccessException {

        JFrame frame = new JFrame();
        PropertyDescriptor[] descriptors = myIntrospector(component);

        Object rowData[][] = new Object[descriptors.length+1][2];
        int it=0;
        for(int i=0;i<descriptors.length;i++){


            try {
                PropertyDescriptor desc = descriptors[i];
                Object name = desc.getPropertyType().getName();
                Object val = desc.getReadMethod().invoke(component);

                if (name.equals("java.lang.Class") || val == null) {
                    continue;
                }

                rowData[it][0] = desc.getName();
                rowData[it++][1] = val;

            }catch (Exception e)
            {

            }
        }

        Object columnNames[] = { "Name", "Value"};
        JTable table = new JTable(rowData, columnNames);


        JScrollPane scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setSize(600,500);
        frame.setLocation(900, 0);
        return frame;

    }

}