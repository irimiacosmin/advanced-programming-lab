package cosmin.java.lab4;

public class MyException extends Exception{

    public MyException(String message){
        super("MyException - " + message);
    }

}
