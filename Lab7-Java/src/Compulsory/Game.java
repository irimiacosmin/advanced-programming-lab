package Compulsory;


import Optional.Evil;
import Optional.Words;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Game {
    private Bag bag;
    private Board board;
    public Words words;
    private Dictionary dictionary;
    private final List<Player> players = new ArrayList<>();
    private final List<Thread> threads = new ArrayList<>();

    public void addPlayer(Player player) {
        players.add(player);
        player.setGame(this);
    }

    public void start() throws InterruptedException {

        words = new Words();

        if(players.size()>4){
            System.err.println("Error 1: Too many players!");
            return;
        }

        Evil evil = new Evil();
        Thread t = new Thread(evil);
        t.setDaemon(true);
        t.start();
        System.out.println("[GAME] STATS - Daemon started");

        for(Player player: players){
            threads.add(new Thread(player));
        }

        for(Thread thread: threads){
            thread.start();
        }

        System.out.println("[GAME] STATS - All threads started");
        evil.addThreadList(threads);


    }

    public synchronized Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Thread> getThreads() {
        return threads;
    }
}