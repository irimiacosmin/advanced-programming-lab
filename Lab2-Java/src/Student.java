import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Student implements Person{

    private String studentID;
    private String name;
    private String surname;
    private String email;
    private int positionInProjectList;
    private int positionInPreferences;
    private Project project;
    private List<Project> preferences;

    public Student(String studentID, String name, String surname, String email) {
        this.studentID = studentID;
        this.name = name;
        this.surname = surname;
        this.email = email;
        positionInPreferences = -1;
        positionInProjectList = -1;
        project = new Project("FOO",0);
        preferences = new ArrayList<Project>();
    }

    @Override
    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Project> getPreferences() {
        return preferences;
    }

    public void setPreferences(Project ... projects) {
        for(Project p: projects)
            preferences.add(p);

    }

    public int getPreferenceSize() {
        return this.preferences.size();
    }

    public int getPositionInProjectList() {
        return positionInProjectList;
    }

    public void setPositionInProjectList(int positionInProjectList) {
        this.positionInProjectList = positionInProjectList;
    }

    public int getPositionInPreferences() {
        return positionInPreferences;
    }

    public void setPositionInPreferences(int positionInPreferences) {
        this.positionInPreferences = positionInPreferences;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentID='" + studentID + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", preferences=" + preferences +
                '}';
    }

    public String getString(){
        if(positionInProjectList > -1)
            return "(" + this.studentID + ", " + this.project.getName() + ")";
        else
            return "(" + this.studentID + ", nealocat)";
    }

    @Override
    public String getTeacherID() {
        return null;
    }

    @Override
    public Project createProject(String name, int capacity) {
        return null;
    }

    public void setPreferences(List<Project> preferences) {
        this.preferences = preferences;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return positionInProjectList == student.positionInProjectList &&
                positionInPreferences == student.positionInPreferences &&
                Objects.equals(studentID, student.studentID) &&
                Objects.equals(name, student.name) &&
                Objects.equals(surname, student.surname) &&
                Objects.equals(email, student.email) &&
                Objects.equals(project, student.project) &&
                Objects.equals(preferences, student.preferences);
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentID, name, surname, email, positionInProjectList, positionInPreferences, project, preferences);
    }


    public String analyzeStud(){
        return "[Student]-{"+this.getStudentID()+"} got project {"+
                this.getProject().getName()+"} witch was option NO "+
                (this.getPositionInPreferences()+1)+" in his preferences list.";
    }

}
