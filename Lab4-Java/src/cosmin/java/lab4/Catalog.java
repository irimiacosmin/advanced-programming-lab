package cosmin.java.lab4;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Catalog implements Serializable {

    List<Document> documentList;


    public Catalog() {
        documentList = new ArrayList<Document>();
    }

    public void add(Document document){

        documentList.add(document);
    }

    public void save(String filename){

        Utils.serializare(filename,this);
    }

    public void load(String filename){
        Catalog catalog = Utils.deserializare(filename);
        this.documentList = catalog.getDocumentList();
    }

    public void open(String filename){

        Utils.openFile(filename);
    }

    public void list(){
        for(Document document: documentList)
            System.out.println(document);
    }

    public List<Document> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

}
