package app;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    public static void main(String args[]) throws IOException, SQLException, ClassNotFoundException {
        new AbstractFactory().run();
    }

}
