package Compulsory;

import Optional.Combination;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Bag {
    private final Queue<Character> letters = new LinkedList<>();


    public Bag() {
        fillTheBag();
    }

    private synchronized void addSomeLetters(int howMany,char c){
        for(int i=1;i<=howMany;i++){
            letters.add(c);
        }
    }
    private synchronized void fillTheBag(){

        addSomeLetters(9,'a');
        addSomeLetters(2,'b');
        addSomeLetters(2,'c');
        addSomeLetters(4,'d');
        addSomeLetters(12,'e');
        addSomeLetters(2,'f');
        addSomeLetters(3,'g');
        addSomeLetters(2,'h');
        addSomeLetters(9,'i');
        addSomeLetters(1,'j');
        addSomeLetters(1,'k');
        addSomeLetters(4,'l');
        addSomeLetters(2,'m');
        addSomeLetters(6,'n');
        addSomeLetters(8,'o');
        addSomeLetters(2,'p');
        addSomeLetters(1,'q');
        addSomeLetters(6,'r');
        addSomeLetters(4,'s');
        addSomeLetters(6,'t');
        addSomeLetters(6,'u');
        addSomeLetters(2,'v');
        addSomeLetters(2,'w');
        addSomeLetters(1,'x');
        addSomeLetters(2,'y');
        addSomeLetters(1,'z');

        Collections.shuffle((List<?>) letters, new Random(System.nanoTime()));
    }

    public synchronized int getSize(){
        return letters.size();
    }

    public synchronized List<Character> extractLetters(int howMany) {
        List<Character> extracted = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            if (letters.isEmpty()) break;
            extracted.add(letters.poll());
        }
        return extracted;
    }

    public synchronized Queue<Character> getLetters() {
        return letters;
    }
}