package Optional;

import Compulsory.Game;
import Compulsory.Player;

import java.util.List;

public class ScrabblePlayer extends Player {

    public ScrabblePlayer(String name) {
        super(name);
    }

    @Override
    public List<Character> getMyLetters() {
        return super.getMyLetters();
    }

    @Override
    public void setMyLetters(List<Character> myLetters) {
        super.setMyLetters(myLetters);
    }

    @Override
    public void run() {
        super.run();
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public Game getGame() {
        return super.getGame();
    }

    @Override
    public void setGame(Game game) {
        super.setGame(game);
    }

    @Override
    public int getPoints() {
        return super.getPoints();
    }

    @Override
    public void setPoints(int points) {
        super.setPoints(points);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public synchronized List<String> getValidWords(List<Character> characterList, int k) {
        return super.getValidWords(characterList, k);
    }

    @Override
    public synchronized void combination(List<String> combinations, Object[] elements, int K) {
        super.combination(combinations, elements, K);
    }

    @Override
    public synchronized int c(int n, int r) {
        return super.c(n, r);
    }

    @Override
    public synchronized int fact(int n) {
        return super.fact(n);
    }

    @Override
    public synchronized void print(List<String> combinations, int[] combination, Object[] elements) {
        super.print(combinations, combination, elements);
    }
}
