package cosmin.java.lab4;

public class Article extends Document implements DocumentManager{

    public String journal;

    public Article(){}

    public Article(String title, String path, int year, String... authors) throws MyException {
        super(title, path, year, authors);
        this.journal = null;
    }

    public Article(String title, String path, String journal, int year, String... authors) throws MyException {
        super(title, path, year, authors);
        this.journal = journal;
    }


    @Override
    public String toString() {
        return "Article{" +
                "journal='" + journal + '\'' +
                ", title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", year=" + year +
                ", authors=" + authors +
                '}';
    }

    @Override
    public void describe() {
        System.out.println("This is a Article description.");

    }
}
