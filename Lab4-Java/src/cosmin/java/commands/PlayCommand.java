package cosmin.java.commands;

import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.MyException;

import java.util.List;

public class PlayCommand implements ShellFace  {

    public List<String> arguments;
    public Catalog catalog;


    public PlayCommand(Catalog catalog, List<String> arguments) throws MyException {
        this.arguments = arguments;
        this.catalog = catalog;

        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException {
        if(arguments.size() < 2)
        {
            throw new MyException("Too few arguments.");
        }
        catalog.open(arguments.get(1));
    }
}
