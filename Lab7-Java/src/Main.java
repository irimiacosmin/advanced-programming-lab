import Compulsory.Bag;
import Compulsory.Board;
import Compulsory.Game;
import Compulsory.Player;
import Optional.ScrabblePlayer;
import Optional.Words;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    public static void main(String args[]) throws InterruptedException {
        Game game = new Game();
        Bag bag = new Bag();
        Board board = new Board();
        Player player1 = new ScrabblePlayer("Player 1");
        Player player2 = new ScrabblePlayer("Player 2");
        Player player3 = new ScrabblePlayer("Player 3");

        game.setBag(bag);
        game.setBoard(board);
        game.addPlayer(player1);
        game.addPlayer(player2);
        game.addPlayer(player3);
        game.start();


        //System.out.println(board.words);

    }

}
