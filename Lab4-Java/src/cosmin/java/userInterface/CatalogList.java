package cosmin.java.userInterface;

import javax.swing.*;

public class CatalogList extends JList {
    DefaultListModel model = new DefaultListModel<>();
    JTable table;

    public CatalogList() {
        String title = "<html><i><font color='blue'>" +
                "Catalog Documents" + "</font></i></hmtl>";

        this.setBorder(BorderFactory.createTitledBorder(title));
        this.setModel(model);


    }
    public void addDocument(String item) {
        model.addElement(item);
    }

    public void removeDocument(int index) {

        DefaultListModel selection = (DefaultListModel) this.getModel();
        selection.remove(index);

    }

    public int getIndex(){
        DefaultListModel selection = (DefaultListModel) this.getModel();
        int selectedIndex = this.getSelectedIndex();
        if (selectedIndex != -1) {
            return selectedIndex;
        }
        return -1;
    }

    public void removeAll(){
        model.removeAllElements();
    }

}
