package controllers;

import classes.Chart;
import classes.Database;

import java.io.*;
import java.sql.*;

public class EntityController {

    public void createChart(Chart chart) throws ClassNotFoundException, SQLException, IOException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("insert into Charts (chart) values (?)")) {
            write(chart,pstmt,1);
            pstmt.execute();
        }
    }

    public Chart readChart(int id) throws SQLException, ClassNotFoundException, IOException {
        Connection con = Database.getConnection();
        Chart chart = null;
        try (Statement stmt = con.createStatement();)
        {
            stmt.executeQuery("select chart from Charts where id="+id);
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) {
                chart = (Chart)read(rs, "chart");
            }
        }
        return chart;
    }

    public void updateChart(int id, Chart chart) throws SQLException, ClassNotFoundException, IOException{
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("UPDATE Charts SET chart = (?) WHERE id ="+id)) {
            write(chart,pstmt,1);
            pstmt.execute();
        }
    }

    public void deleteChart(int id) throws SQLException, ClassNotFoundException, IOException{
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("delete from Charts where id="+id)) {
            pstmt.executeUpdate();
        }
    }


    public void clear() throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("delete from Charts where id >0")) {
            pstmt.executeUpdate();
        }
        try (PreparedStatement pstmt = con.prepareStatement("ALTER TABLE Charts AUTO_INCREMENT = 1")) {
            pstmt.executeUpdate();
        }
    }

    private void write(Object obj, PreparedStatement ps, int parameterIndex) throws SQLException, IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oout = new ObjectOutputStream(baos);
        oout.writeObject(obj);
        oout.close();
        ps.setBytes(parameterIndex, baos.toByteArray());
    }

    private Object read(ResultSet rs, String column) throws SQLException, IOException, ClassNotFoundException {
        byte[] buf = rs.getBytes(column);
        if (buf != null) {
            ObjectInputStream objectIn = new ObjectInputStream(
                    new ByteArrayInputStream(buf));
            return objectIn.readObject();
        }
        return null;
    }

}
