package controllers;

import classes.Database;

import java.sql.*;

public class OldAlbumController {

    private OldArtistController artistController;

    public void create(int artist_id, String name, int release_year) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("insert into albums (name, artist_id, release_year) values (?, ?, ?)")) {
            pstmt.setString(1, name);
            pstmt.setInt(2, artist_id);
            pstmt.setInt(3, release_year);
            pstmt.executeUpdate();
        }
    }

    public void list(int radioheadId) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();)
        {
            stmt.executeQuery("select name, release_year from albums where artist_id=" + radioheadId );
            ResultSet rs = stmt.getResultSet();
            int cat;

            while(rs.next()) {
                cat = rs.getRow();
                System.out.println("     -> Album name: " + rs.getString(1) + ", relased in " + rs.getInt(2));
            }
        }
    }

    public void clear() throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("delete from albums where id >0")) {
            pstmt.executeUpdate();
        }
        try (PreparedStatement pstmt = con.prepareStatement("ALTER TABLE albums AUTO_INCREMENT = 1")) {
            pstmt.executeUpdate();
        }
    }



    public void setArtistController(OldArtistController artistController){
        this.artistController = artistController;
    }

    public Integer findByName(String name) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();) {
            stmt.executeQuery("select id from albums where name='" + name + "'");
            ResultSet rs = stmt.getResultSet();
            return rs.next() ? rs.getInt(1) : null;
        }
    }

    public Integer getArtistID(int id) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();) {
            stmt.executeQuery("select artist_id from albums where id=" + id );
            ResultSet rs = stmt.getResultSet();
            return rs.next() ? rs.getInt(1) : null;
        }
    }

}
