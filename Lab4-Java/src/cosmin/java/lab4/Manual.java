package cosmin.java.lab4;

public class Manual extends Document implements DocumentManager{

    public Manual(){}

    public Manual(String title, String path, int year, String... authors) throws MyException {
        super(title, path, year, authors);
    }

    @Override
    public String toString() {
        return "Manual{" +
                "title='" + title + '\'' +
                ", path='" + path + '\'' +
                ", year=" + year +
                ", authors=" + authors +
                '}';
    }

    @Override
    public void describe() {
        System.out.println("This is a Manual description.");
    }
}
