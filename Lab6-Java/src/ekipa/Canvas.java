package ekipa;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import shapes.RegularPolygon;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.ArrayList;
import java.util.Random;

public class Canvas extends JPanel {

    public Toolbar toolbar;
    public ControlPanel controlPanel;
    public Graphics2D graphics;
    public BufferedImage image;
    BufferedImage img = null;
    public ArrayList<BufferedImage> bufferedImageArrayList;
    public int radius;
    public int sides;
    public int x,y;
    Color myColour;
    public boolean transparency = false;
    final static float dash1[] = {10.0f};
    final static BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);

    public Canvas(){

        bufferedImageArrayList = new ArrayList<BufferedImage>();

        this.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int noOfSd = Integer.parseInt(toolbar.sidesNo.getValue().toString());
                int size = Integer.parseInt(toolbar.shapesSize.getValue().toString());
                int stroke = toolbar.strokeList.getSelectedIndex();
                boolean isTrans = toolbar.transparentCB.isSelected();

                drawShapeAt(e.getX(), e.getY(),size,noOfSd,stroke+1,isTrans);
            }
        });

    }

    public void drawShapeAt(int x, int y, int radius, int sides,int stroke,boolean transparency) {
        this.radius = radius;
        this.sides = sides;
        Polygon polygon = new RegularPolygon(x, y, radius, sides);

        image = new BufferedImage(900,700, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics = image.createGraphics();
        bufferedImageArrayList.add(image);
        Random rand = new Random();


        if(transparency == true) {
            myColour = new Color(rand.nextInt(255), rand.nextInt(255), rand.nextInt(255), 127);
        }
        else {
            myColour = new Color(rand.nextInt(0xFFFFFF));
        }


        switch (stroke){

            case 1: {
                graphics.setColor(myColour);
                graphics.fill(polygon);
                break;
            }
            case 2:{
                graphics.setStroke(new BasicStroke(3.0f));
                graphics.setPaint(myColour);
                graphics.draw(polygon);
                break;
            }
            case 3:{
                graphics.setStroke(dashed);
                graphics.setPaint(myColour);
                graphics.draw(polygon);
                break;
            }
            default:
                graphics.fill(polygon);
        }

    }

    public Dimension getPreferredSize() {
            return new Dimension(900,700);
    }

    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (img!=null) {
            g.drawImage(img, 0, 0, this);
        }

        for(BufferedImage bf: bufferedImageArrayList){

            g.drawImage(bf, x, y, new ImageObserver() {
                @Override
                public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
                    return false;
                }
            });
        }
        repaint();
    }

    public void setToolbar(Toolbar toolbar){
        this.toolbar = toolbar;
    }

    public void setControlPanel(ControlPanel controlPanel){
        this.controlPanel = controlPanel;
    }


    
}