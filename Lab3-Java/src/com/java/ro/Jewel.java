package com.java.ro;

public class Jewel extends Item {

    public Jewel(String name, double price) {
        super(name, price);
    }

    @Override
    public String toString() {
        return "Jewel{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
