package functions;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.jfree.graphics2d.svg.SVGGraphics2D;
import org.jfree.graphics2d.svg.SVGUtils;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import Jama.Matrix;


public class FunctionsControlPanel extends JPanel{

    FunctionCanvas functionCanvas;

    public JButton drawBtn = new JButton("Draw");
    public JButton plotBtn = new JButton("Plot");
    public JButton interBtn = new JButton("Interpolate");
    public JButton saveBtn = new JButton("Save");
    public JButton resetBtn = new JButton("Reset");
    public JLabel fLabel = new JLabel("f(x):");
    public JTextField textField = new JTextField();
    private int clickCount = 0;


    public FunctionsControlPanel(FunctionCanvas functionCanvas){
        this.functionCanvas = functionCanvas;
        plotBtn.setBackground(Color.white);
        interBtn.setBackground(Color.white);
        drawBtn.setBackground(Color.white);
        saveBtn.setBackground(Color.white);
        resetBtn.setBackground(Color.white);
        add(plotBtn);
        add(interBtn);
        add(fLabel);
        add(textField);
        add(drawBtn);
        add(resetBtn);
        add(saveBtn);

        textField.setPreferredSize( new Dimension( 250, 24 ) );

        drawBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                plotBtn.setBackground(Color.white);
                plotBtn.setOpaque(false);
                functionCanvas.setManualPlot(false);
                clickCount++;
                functionCanvas.setManualPlot(false);
                String expression = textField.getText().toLowerCase().replaceAll("\\s+","");
                functionCanvas.drawExpression(expression);

            }
        });


        plotBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clickCount++;
                if(clickCount%2==1){
                    plotBtn.setBackground(Color.GREEN);
                    plotBtn.setOpaque(true);
                    functionCanvas.setManualPlot(true);
                }else{
                    plotBtn.setBackground(Color.white);
                    plotBtn.setOpaque(false);
                    functionCanvas.setManualPlot(false);
                }

            }
        });

        saveBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                display();
            }
        });

        resetBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                functionCanvas.functionPanel.setVisible(false);
                functionCanvas.functionPanel = new FunctionPanel();
            }
        });

        interBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(functionCanvas.manualPlot == false || functionCanvas.points.size()<3){
                    JOptionPane.showMessageDialog(functionCanvas, "You need to draw first at least 3 points!");
                }else{
                    functionCanvas.activateInterpolateMode();
                }

            }
        });

    }

    public void save(String filename, String extension){

        BufferedImage bImg = new BufferedImage(functionCanvas.getWidth(), functionCanvas.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics2D cg = bImg.createGraphics();
        functionCanvas.paintAll(cg);
        try {
            ImageIO.write(bImg, extension, new File(filename+"."+extension));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void saveSVG(String filename){

        SVGGraphics2D graphics = new SVGGraphics2D(functionCanvas.getWidth(), functionCanvas.getHeight());
        functionCanvas.paintAll(graphics);

        try {
            SVGUtils.writeToSVG(new File(filename+".svg"), graphics.getSVGElement());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void display() {
        JTextField field1 = new JTextField("");
        String[] ext = { "png", "jpeg", "bmp","svg"};
        System.out.println(ext[1]);
        JComboBox extList = new JComboBox(ext);

        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel("Filename:"));
        panel.add(field1);

        panel.add(new JLabel("Extension:"));
        panel.add(extList);

        int result = JOptionPane.showConfirmDialog(null, panel, "Filename",
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {

            String filename = field1.getText().replaceAll("\\s+","");

            if(filename.equals("") || filename.equals(" "))
                JOptionPane.showMessageDialog(this, "Filename is not valid!");
            else {
                if(extList.getSelectedIndex() == 3){
                    saveSVG(filename);
                }
                else {
                    save(filename, ext[extList.getSelectedIndex()]);
                }
            }

        } else {
            System.out.println("Cancelled");
        }
    }


    //http://geosoft.no/software/lagrange/LagrangeInterpolation.java.html
    public static double[] findPolynomialFactors (double[] x, double[] y) throws RuntimeException
    {
        int n = x.length;

        double[][] data = new double[n][n];
        double[]   rhs  = new double[n];

        for (int i = 0; i < n; i++) {
            double v = 1;
            for (int j = 0; j < n; j++) {
                data[i][n-j-1] = v;
                v *= x[i];
            }

            rhs[i] = y[i];
        }

        Matrix m = new Matrix (data);
        Matrix b = new Matrix (rhs, n);
        Matrix s = m.solve (b);
        return s.getRowPackedCopy();
    }

}
