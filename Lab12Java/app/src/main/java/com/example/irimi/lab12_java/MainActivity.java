package com.example.irimi.lab12_java;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class MainActivity extends AppCompatActivity {

    private Context context = null;

    private Button btn_do_it;
    private Button btn_date;
    private Button btn_time;

    private EditText tb_name;
    private EditText tb_details;
    private EditText tb_date;
    private ListView lv_events;

    private List<Event> eventList;
    private ArrayAdapter<Event> adapter;
    private PendingIntent pendingIntent;

    private int mYear, mMonth, mDay, mHour, mMinute;
    private int aYear = -1, aMonth = -1, aDay = -1, aHour = -1, aMinute = -1;
    private boolean isToday = false;

    private void init(){
        eventList = new ArrayList<Event>();
        loadList();
        btn_do_it = (Button) findViewById(R.id.btn_do_it);
        btn_date = (Button) findViewById(R.id.btn_date);
        btn_time = (Button) findViewById(R.id.btn_time);
        lv_events = (ListView) findViewById(R.id.lv_events);
        tb_name = (EditText) findViewById(R.id.tb_name);
        tb_details = (EditText) findViewById(R.id.tb_details);
        tb_date = (EditText) findViewById(R.id.tb_date_1);

        adapter = new ArrayAdapter<Event>(context, android.R.layout.simple_list_item_1, eventList);
        lv_events.setAdapter(adapter);

    }

    private String getText(EditText editText){
        String aux = editText.getText().toString();
        if(aux.equals("")){
            return null;
        }
        return aux;
    }


    private void wakeMe(Event event){

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MONTH,event.getMonth()-1);
        cal.set(Calendar.YEAR,event.getYear());
        cal.set(Calendar.DAY_OF_MONTH,event.getDay());
        cal.set(Calendar.HOUR_OF_DAY,event.getHour());
        cal.set(Calendar.MINUTE,event.getMinute());
        AlarmNotificationReceiver.event = event;
        startAlarm(cal,event);
    }

    private void startAlarm(Calendar cal, Event event) {

        AlarmManager manager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent myIntent;
        PendingIntent pendingIntent;
        myIntent = new Intent(MainActivity.this,AlarmNotificationReceiver.class);
        myIntent.putExtra("event_title",  event.getName());
        myIntent.putExtra("event_details",  event.getDetails());
        pendingIntent = PendingIntent.getBroadcast(this,0,myIntent,0);
        manager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),pendingIntent);

    }

    private void saveList(){
        SharedPreferences sharedPreferences =
                getSharedPreferences("shared preferences",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(eventList);
        editor.putString("list",json);
        editor.apply();
    }

    private void loadList(){
        SharedPreferences sharedPreferences =
                getSharedPreferences("shared preferences",MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("list",null);
        Type type = new TypeToken<ArrayList<Event>>(){}.getType();
        eventList = gson.fromJson(json,type);
        if(eventList == null){
            eventList = new ArrayList<Event>();
        }


    }


    private void do_it(){


        btn_do_it.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = getText(tb_name);
                String details = getText(tb_details);
                if(name == null){Toast.makeText(context,"Event name is empty.",Toast.LENGTH_SHORT).show();}
                else if(details == null){Toast.makeText(context,"Event details is empty.",Toast.LENGTH_SHORT).show();}
                else if(aYear == -1 || aMonth == -1|| aDay == -1|| aHour == -1|| aMinute == -1){Toast.makeText(context,"Event date is not complete.",Toast.LENGTH_SHORT).show();}
                else{
                    Event event = new Event(name,details,aYear,aMonth,aDay,aHour,aMinute);
                    eventList.add(event);
                    wakeMe(event);
                    saveList();
                    adapter.notifyDataSetChanged();
                    tb_date.setText("Date");
                    tb_details.setText("");
                    tb_name.setText("");
                    aYear = aMonth = aDay = aHour = aMinute = -1;
                }

            }
        });

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                aYear = year;
                                aMonth = monthOfYear + 1;
                                aDay = dayOfMonth;
                                if(aYear > mYear){

                                    if(aMinute == -1 || aHour == -1){
                                        tb_date.setText(aDay+"-"+aMonth+"-"+aYear);
                                    }else{
                                        tb_date.setText(aDay+"-"+aMonth+"-"+aYear+" "+aHour+":"+aMinute);
                                    }

                                }else if(aYear == mYear){

                                    if(aMonth > mMonth + 1){
                                        if(aMinute == -1 || aHour == -1){
                                            tb_date.setText(aDay+"-"+aMonth+"-"+aYear);
                                        }else{
                                            tb_date.setText(aDay+"-"+aMonth+"-"+aYear+" "+aHour+":"+aMinute);
                                        }

                                    }else if(aMonth == mMonth+1){
                                        if(aDay >= mDay){

                                            if(aMinute == -1 || aHour == -1){
                                                tb_date.setText(aDay+"-"+aMonth+"-"+aYear);
                                            }else{
                                                tb_date.setText(aDay+"-"+aMonth+"-"+aYear+" "+aHour+":"+aMinute);
                                            }

                                        }else{
                                            aYear = -1;aMonth = -1;aDay = -1;
                                            Toast.makeText(context,"The day is in the past.",Toast.LENGTH_LONG).show();
                                        }
                                    }else{
                                        aYear = -1;aMonth = -1;aDay = -1;
                                        Toast.makeText(context,"The month is in the past.",Toast.LENGTH_LONG).show();
                                    }
                                }else {
                                    aYear = -1;aMonth = -1;aDay = -1;
                                    Toast.makeText(context, "The year is in the past.", Toast.LENGTH_LONG).show();
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(aYear == -1 || aMonth == -1 || aDay == -1) {
                    Toast.makeText(context, "Please set the date first. ",Toast.LENGTH_LONG).show();
                }
                else {
                    final Calendar c = Calendar.getInstance();
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH) +1;
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                    mHour = c.get(Calendar.HOUR_OF_DAY);
                    mMinute = c.get(Calendar.MINUTE);

                    //String str = mYear + " " + aYear + " - " +mMonth + " " + aMonth + " - "+mDay + " " + aDay + " - ";


                    if(mYear == aYear && mMonth == aMonth && mDay == aDay){
                        isToday = true;

                    }
                    else{
                        isToday = false;
                    }

                    TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                            new TimePickerDialog.OnTimeSetListener() {
                                @Override
                                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                    aHour = hourOfDay;
                                    aMinute = minute;

                                    if(isToday == false){
                                        if (aYear == -1 || aMonth == -1 || aDay == -1) {
                                            tb_date.setText(aHour + ":" + aMinute);
                                        } else {
                                            tb_date.setText(aDay + "-" + aMonth + "-" + aYear + " " + aHour + ":" + aMinute);
                                        }
                                    }
                                    else{
                                    if (aHour > mHour) {

                                        if (aYear == -1 || aMonth == -1 || aDay == -1) {
                                            tb_date.setText(aHour + ":" + aMinute);
                                        } else {
                                            tb_date.setText(aDay + "-" + aMonth + "-" + aYear + " " + aHour + ":" + aMinute);
                                        }

                                    } else if (aHour == mHour) {

                                        if (aMinute > mMinute) {
                                            if (aYear == -1 || aMonth == -1 || aDay == -1) {
                                                tb_date.setText(aHour + ":" + aMinute);
                                            } else {
                                                tb_date.setText(aDay + "-" + aMonth + "-" + aYear + " " + aHour + ":" + aMinute);
                                            }
                                        } else {
                                            aMinute = -1;
                                            aHour = -1;
                                            Toast.makeText(context, "The minute is in the past.", Toast.LENGTH_LONG).show();
                                        }

                                    } else {
                                        aMinute = -1;
                                        aHour = -1;
                                        Toast.makeText(context, "The hour is in the past.", Toast.LENGTH_LONG).show();
                                    }


                                    }
                                }
                            }, mHour, mMinute, false);
                    timePickerDialog.show();
                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        init();
        do_it();

    }

}
