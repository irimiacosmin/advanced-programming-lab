package com.java.ro;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Utils {

    public static List<Item> sortByPrice(List<Item> assetList){

        Collections.sort(assetList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                if(o1.price == o2.price) return 0;
                if(o1.price > o2.price) return 1;
                return -1;
            }
        });

        return assetList;
    }

    public static List<Item> sortByPriceDesc(List<Item> assetList){

        Collections.sort(assetList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                if(o1.price == o2.price) return 0;
                if(o1.price > o2.price) return -1;
                return 1;
            }
        });

        return assetList;
    }

    public static List<Item> sortByProfit(List<Item> assetList){

        Collections.sort(assetList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                double profitA = ((Asset) o1).computeProfit() ;
                double profitB = ((Asset) o2).computeProfit() ;
                if(profitA == profitB) return 0;
                if(profitA > profitB) return 1;
                return -1;
            }
        });

        return assetList;
    }

    public static List<Item> sortByProfitDesc(List<Item> assetList){

        Collections.sort(assetList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                double profitA = ((Asset) o1).computeProfit() ;
                double profitB = ((Asset) o2).computeProfit() ;
                if(profitA == profitB) return 0;
                if(profitA > profitB) return -1;
                return 1;
            }
        });

        return assetList;
    }

}
