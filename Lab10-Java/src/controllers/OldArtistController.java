package controllers;

import classes.Database;

import java.sql.*;

public class OldArtistController {

    public OldArtistController() {
    }

    public void create(String name, String country) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("insert into artists (name, country) values (?, ?)")) {
            pstmt.setString(1, name);
            pstmt.setString(2, country);
            pstmt.executeUpdate();
        }
    }

    public String findById(int id) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();)
        {
            stmt.executeQuery("select NAME from artists where id=" + id );
            ResultSet rs = stmt.getResultSet();
            return rs.next() ? rs.getString(1) : null;
        }
    }

    public Integer findByName(String name) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();) {
            stmt.executeQuery("select id from artists where name='" + name + "'");
            ResultSet rs = stmt.getResultSet();
            return rs.next() ? rs.getInt(1) : null;
        }
    }

    public Integer findByNameLast(String name) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();) {
            stmt.executeQuery("select id from artists where name='" + name + "' order by id desc");
            ResultSet rs = stmt.getResultSet();
            return rs.next() ? rs.getInt(1) : null;
        }
    }

    public void clear() throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("delete from artists where id >0")) {
            pstmt.executeUpdate();
        }
        try (PreparedStatement pstmt = con.prepareStatement("ALTER TABLE artists AUTO_INCREMENT = 1")) {
            pstmt.executeUpdate();
        }
    }

}