import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Problem {

    private List<Student> studentsList;
    private List<Teacher> teachersList;

    private boolean solved;

    private int howManyStudents;
    private int howManyTeachers;

    public Problem(){

        studentsList = new ArrayList<Student>();
        teachersList = new ArrayList<Teacher>();

    }

    public List<Person> getParticipants(){
        List<Person> participants = new ArrayList<Person>();
        participants.addAll(studentsList);
        participants.addAll(teachersList);
        return participants;
    }


    public void setStudents(Student ... students){
        for(Student s:students)
            studentsList.add(s);
        this.howManyStudents = studentsList.size();
    }

    public void setTeachers(Teacher ... teachers){
        for(Teacher t:teachers)
            teachersList.add(t);
        this.howManyTeachers = teachersList.size();
    }


    public int allocateStudentToProject(Student student){

        Project project;
        int prefPosition, projPrefPosition;

        List<Project> studentPref = student.getPreferences();
        List<Student> projectPref;
        prefPosition = student.getPositionInPreferences() + 1;

        // iterez prin lista de preferinte a studentului
        for(; prefPosition < studentPref.size(); ++prefPosition ){
            student.setPositionInPreferences(prefPosition);
            project = studentPref.get(prefPosition);
            projectPref = project.getPreferences();
            projPrefPosition = project.getLastPostion() - 1;

            // iterez prin lista de preferinte a proiectului curent
            for(; projPrefPosition >=0 ; projPrefPosition--){
                if(projectPref.get(projPrefPosition).equals(student)){
                    project.incrementNrStudsAllocated();

                    if(project.getNrStudsAllocated() > project.getCapacity())
                        projectPref.get(project.getLastPostion()).setPositionInProjectList(-1);

                    if(project.getNrStudsAllocated() == project.getCapacity())
                        project.setLastPostion(projPrefPosition);

                    student.setProject(project);
                    return projPrefPosition;
                }
            }
        }

        return -2;
    }

    public void solve(){

        boolean ok = true;
        while(ok){
            ok = false;

            for(Student student : studentsList){
                if(student.getPositionInProjectList() > -1)
                    continue;

                student.setPositionInProjectList(allocateStudentToProject(student));
                if(student.getPositionInProjectList() > -1)
                    ok = true;
            }
        }
        solved = true;
    }

    @Override
    public String toString() {
        String print = "";
        solve();

        if(!solved)
            return "ERROR: problem cannot be solved.";
        else {
            for(Student student : studentsList)
                print += student.getString()+ "\n";
        }
        return print;
    }

    public List<Student> getStudentsList() {
        return studentsList;
    }

    public void setStudentsList(List<Student> studentsList) {
        this.studentsList = studentsList;
    }

    public List<Teacher> getTeachersList() {
        return teachersList;
    }

    public void setTeachersList(List<Teacher> teachersList) {
        this.teachersList = teachersList;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }

    public int getHowManyStudents() {
        return howManyStudents;
    }

    public void setHowManyStudents(int howManyStudents) {
        this.howManyStudents = howManyStudents;
    }

    public int getHowManyTeachers() {
        return howManyTeachers;
    }

    public void setHowManyTeachers(int howManyTeachers) {
        this.howManyTeachers = howManyTeachers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Problem problem = (Problem) o;
        return solved == problem.solved &&
                howManyStudents == problem.howManyStudents &&
                howManyTeachers == problem.howManyTeachers &&
                Objects.equals(studentsList, problem.studentsList) &&
                Objects.equals(teachersList, problem.teachersList);
    }

    @Override
    public int hashCode() {

        return Objects.hash(studentsList, teachersList, solved, howManyStudents, howManyTeachers);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }


}
