package custom.button;

public @interface Width {

    int value() default 0;

}
