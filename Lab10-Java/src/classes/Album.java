package classes;

import java.io.Serializable;

public class Album implements Serializable {

    int id;
    String name;
    int relase_year;

    public Album(int id, String name, int relase_year) {
        this.id = id;
        this.name = name;
        this.relase_year = relase_year;
    }

    @Override
    public String toString() {
        return "Album{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", country='" + relase_year + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRelase_year() {
        return relase_year;
    }

    public void setRelase_year(int relase_year) {
        this.relase_year = relase_year;
    }
}
