package cosmin.java.commands;

import cosmin.java.lab4.Catalog;
import cosmin.java.lab4.MyException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

public class InfoCommand implements ShellFace {

    public List<String> arguments;
    public Catalog catalog;

    public InfoCommand(List<String> arguments, Catalog catalog) throws MyException, IOException {
        this.arguments = arguments;
        this.catalog = catalog;
        computeCommand(arguments);
    }

    @Override
    public void computeCommand(List<String> arguments) throws MyException, IOException {

        String objectType = arguments.get(1);

        //html, pdf, xls

        if(arguments.size() < 2)
        {
            throw new MyException("Too few arguments.");
        }
        printData(objectType);


    }

    public void printData(String filePath) throws IOException {

        Path file = Paths.get(filePath);
        BasicFileAttributes attr = Files.readAttributes(file, BasicFileAttributes.class);

        System.out.println("creationTime: " + attr.creationTime());
        System.out.println("lastAccessTime: " + attr.lastAccessTime());
        System.out.println("lastModifiedTime: " + attr.lastModifiedTime());

        System.out.println("isDirectory: " + attr.isDirectory());
        System.out.println("isOther: " + attr.isOther());
        System.out.println("isRegularFile: " + attr.isRegularFile());
        System.out.println("isSymbolicLink: " + attr.isSymbolicLink());
        System.out.println("size: " + attr.size());

    }

}
