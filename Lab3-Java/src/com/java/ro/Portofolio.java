package com.java.ro;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Portofolio {

    List<Item> itemList;

    public Portofolio(List<Item> itemList) {
        this.itemList = itemList;
    }


    @Override
    public String toString() {
        //itemList = Utils.sortByPrice(itemList);
        return "Portofolio{" +
                "itemList=" + itemList +
                '}';
    }
}
